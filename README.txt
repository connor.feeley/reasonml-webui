Install npm and avahi headers:
# apt install npm libavahi-compat-libdnssd-dev

Install nvm (node version manager):
From: https://github.com/nvm-sh/nvm
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.0/install.sh | bash

$ nvm install 11.13.0

All commands hereafter must be run from the top level of the reasonml-webui repository.

Update the board target:
Edit src/JSONRequest.re: the 'ip' and 'port' variables must point to the target board.

To build:

$ mkdir build

$ npm install

$ npm run build && npm run webpack

Output files are placed in build directory. Due to a hardcoded path, these files must currently be placed in a directory called build (relative to the web root) on the target.

Run the server locally (port 8000):
$ cd build && python3 -m http.server
