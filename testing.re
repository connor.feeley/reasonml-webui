type deviceType =
  | Bach
  | Iggy
  | Newt
  | Raptor
  | Unknown;

let deviceToString = device =>
  switch (device) {
  | Bach => "Bach"
  | Iggy => "Iggy"
  | Newt => "Newt"
  | Raptor => "Raptor"
  | Unknown => "Unknown"
  };

/*
 type getDeviceResult =
   | Success
   | Loading
   | Failure;

 let printDevice = (result, device) =>
   if (result == Success) {
     deviceToString(device);
   } else {
     "Request Failed!";
   };
   */

type getDeviceResult =
  | Success(deviceType)
  | Loading
  | Failure;

let printDeviceBetter = result =>
  switch (result) {
  | Success(device) => deviceToString(device)
  | Loading => "Device type still loading"
  | Failure => "Request Failed"
  };

let fizzbuzz = i =>
  switch (i mod 3, i mod 5) {
  | (0, 0) => "FizzBuzz"
  | (0, _) => "Fizz"
  | (_, 0) => "Buzz"
  | (_, _) => string_of_int(i)
  };

let rec listInRange = (start, end_) =>
  start > end_ ? [] : [start, ...listInRange(start + 1, end_)];

listInRange(0, 100) |> List.map(fizzbuzz);

type option('a) =
  | None
  | Some('a);

let getHeadOfList = (l: list('a)) =>
  switch (l) {
  | [] => None
  | [head, ...rest] => Some(head)
  };
let printDevice = (result, device) =>
  if (result == Success) {
    deviceToString(device);
  } else {
    "Request Failed!";
  };

let test: deviceType = Iggy;

<NumberInput ~label="Test Label"/>