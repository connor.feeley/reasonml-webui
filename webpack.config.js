const path = require('path');
const outputDir = path.join(__dirname, "build/");

const isProd = process.env.NODE_ENV === 'production';

module.exports = {
  entry: './lib/js/src/Index.bs.js',
  mode: isProd ? 'production' : 'development',
  output: {
    path: outputDir,
    publicPath: './',
    filename: 'Index.js',
  },
  resolve: {
    alias: {
      Src: path.resolve(__dirname, 'src/'),
      Images: path.resolve(__dirname, 'src/res/'),
      Styles: path.resolve(__dirname, 'src/styles/'),
      API: path.resolve(__dirname, 'src/api/')
    }
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          { loader: "style-loader" },
          { loader: "css-loader" }
        ]
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          { loader: "file-loader" }
        ]
      },
      {
        test: /\.(jsx)$/,
        use: [
          { loader: "babel-loader" }
        ],
      },
      {
        type: 'javascript/auto',
        test: /\.json$/,
        use: [
          { loader: "file-loader" }
        ]
      }
    ]
  },
  serve: {
    open: true,
    content: './build'
  }
};
