let component = ReasonReact.statelessComponent("SubmitButton");

let make = (~label: string, _children) => {
  ...component,
  render: _self =>
    <div className="form-group row">
      <div className="col-sm-4 m-x-auto">
        <a className="btn btn-outline-secondary">
          {ReasonReact.string(label)}
        </a>
      </div>
    </div>,
};