let component = ReasonReact.statelessComponent("TableDisplay");

let make = _children => {
  ...component,
  render: _self =>
    <table className="table table-hover">
      <tbody> {_children |> ReasonReact.array} </tbody>
    </table>,
};