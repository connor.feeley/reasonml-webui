let component = ReasonReact.statelessComponent("SyncPanel");

let make = _children => {
  ...component,
  render: self =>
    <div>
      <div
        className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1> {ReasonReact.string("Sync")} </h1>
      </div>
      <div className="row wrapper wrapper-content animated fadeInRight">
        <InfoPane title="PTP Status"> <PtpStatus /> </InfoPane>
        <InfoPane title="Interface Status"> <PtpInterfaceStatus /> </InfoPane>
        <InfoPane title="System Clock"> <SystemClock /> </InfoPane>
      </div>
    </div>,
  /*
   <InfoPane title="Configuration">
     <NumberInput label="Domain" />
     <NumberInput label="Priority 1" />
     <SubmitButton label="Apply Configuration" />
   </InfoPane>
   */
};