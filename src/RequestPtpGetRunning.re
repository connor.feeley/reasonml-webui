type ptp_get_running_resp = {
  auxmsg: option(string),
  command: string,
  error: int,
  error_string: string,
  ptp: ptp_get_running_ptp_resp,
  success: bool,
}
and ptp_get_running_ptp_resp = {
  cfgFilePath: string,
  domainNumber: option(int),
  priority1: option(int),
  profile: string,
  slaveOnly: int,
};

type command('a) = {.. decode: Js.Json.t => 'a} as 'a;

type request_state =
  | NotAsked
  | Loading
  | Failure
  | Success(ptp_get_running_resp);

module Decode = {
  let ptp_get_running_ptp_resp = json =>
    Json.Decode.{
      cfgFilePath: json |> field("cfgFilePath", string),
      domainNumber: json |> optional(field("domainNumber", int)),
      priority1: json |> optional(field("priority1", int)),
      profile: json |> field("profile", string),
      slaveOnly: json |> field("slaveOnly", int),
    };
  let ptp_get_running_resp = json =>
    Json.Decode.{
      auxmsg: json |> field("auxmsg", optional(string)),
      command: json |> field("command", string),
      error: json |> field("error", int),
      error_string: json |> field("error_string", string),
      ptp: json |> field("ptp", ptp_get_running_ptp_resp),
      success: json |> field("success", bool),
    };
};

module Encode = {
  let encode_ptp_get_running_ptp_resp = (m: ptp_get_running_ptp_resp) =>
    Json.Encode.(
      object_([
        ("cfgFilePath", m.cfgFilePath |> string),
        (
          "domainNumber",
          switch (m.domainNumber) {
          | Some(json) => json |> int
          | None => null
          },
        ),
        (
          "priority1",
          switch (m.priority1) {
          | Some(json) => json |> int
          | None => null
          },
        ),
        ("profile", m.profile |> string),
        ("slaveOnly", m.slaveOnly |> int),
      ])
    );
  let encode_ptp_get_running_resp = (m: ptp_get_running_resp) =>
    Json.Encode.(
      object_([
        (
          "auxmsg",
          switch (m.auxmsg) {
          | Some(json) => json |> string
          | None => null
          },
        ),
        ("command", m.command |> string),
        ("error", m.error |> int),
        ("error_string", m.error_string |> string),
        ("ptp", m.ptp |> encode_ptp_get_running_ptp_resp),
        ("success", m.success |> bool),
      ])
    );
};

/* makes a post request with the following json payload { hello: "world" } */

let req = (command: string) => {
  /*
   let payload = Js.Dict.empty();
   Js.Dict.set(payload, "\"command\"", Js.Json.string("ptp_get"));
   Js.Dict.set(payload, "\"json\"", Js.Json.object_(Js.Dict.empty()));
     */
  let payload = "{\"command\":\"" ++ command ++ "\", \"json\":{}}";
  Js.log(payload);

  let url = "http://10.232.21.40/cgi-bin/handleCommands";
  Js.Promise.(
    Fetch.fetchWithInit(
      url,
      Fetch.RequestInit.make(
        ~method_=Post,
        /*
         ~body=Fetch.BodyInit.make(Js.Json.stringify(Js.Json.object_(payload))),
          */
        ~body=Fetch.BodyInit.make(payload),
        ~headers=Fetch.HeadersInit.make({"Content-Type": "application/json"}),
        (),
      ),
    )
    |> then_(Fetch.Response.json)
    |> then_(json =>
         json
         |> Decode.ptp_get_running_resp
         |> (resp => Some(resp) |> resolve)
       )
  );
};

type action =
  | LoadPTP
  | LoadedPTP(ptp_get_running_resp)
  | LoadPTPFailed;

type state =
  | NotAsked
  | Loading
  | Failure
  | Success(ptp_get_running_resp);

let component = ReasonReact.reducerComponent("JSONRequest");

let make = _children => {
  ...component,
  initialState: () => NotAsked,
  reducer: (action, _state) =>
    switch (action) {
    | LoadPTP =>
      ReasonReact.UpdateWithSideEffects(
        Loading,
        (
          self =>
            Js.Promise.(
              req("ptp_get_running")
              |> then_(result =>
                   switch (result) {
                   | Some(json) => resolve(self.send(LoadedPTP(json)))
                   | None => resolve(self.send(LoadPTPFailed))
                   }
                 )
              |> ignore
            )
        ),
      )
    | LoadedPTP(json) => ReasonReact.Update(Success(json))
    | LoadPTPFailed => ReasonReact.Update(Failure)
    },
  render: self =>
    switch (self.state) {
    | NotAsked =>
      <div>
        <a
          className="btn btn-outline-primary"
          onClick=(_event => self.send(LoadPTP))>
          {ReasonReact.string("Load PTP")}
        </a>
      </div>
    | Loading => <div> {ReasonReact.string("Loading...")} </div>
    | Failure => <div> {ReasonReact.string("Failed")} </div>
    | Success(json) =>
      <div>
        {
          ReasonReact.string(
            json |> Encode.encode_ptp_get_running_resp |> Json.stringify,
          )
        }
      </div>
    },
};