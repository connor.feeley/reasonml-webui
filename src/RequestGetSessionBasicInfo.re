open CommonTypes;

type action('a) =
  | Load
  | Delete
  | Loaded('a)
  | LoadFailed;

type state =
  | NotAsked
  | Deleted
  | Loading
  | Failure
  | Success(GetSessionBasicInfo.t);

let component = ReasonReact.reducerComponent("GetSessionBasicInfo");

let make = (~session_type: session_type, ~session_id: int, _children, ~render) => {
  ...component,
  initialState: () => NotAsked,
  reducer: (action, _state) =>
    switch (action) {
    | Load =>
      ReasonReact.UpdateWithSideEffects(
        Loading,
        (
          self =>
            Js.Promise.(
              JSONRequest.jsonRequest(
                ~command="get_session_basic_info",
                ~arguments=
                  Json.Encode.(
                    object_([("id", Json.Encode.int(session_id))])
                  ),
                ~decoder=GetSessionBasicInfo.decode,
                (),
              )
              |> Js.Promise.then_(result =>
                   switch (result) {
                   /* If we successfully got a value, request detailed information for each session */
                   | Some(value) => resolve(self.send(Loaded(value)))
                   | None => resolve(self.send(LoadFailed))
                   }
                 )
              |> ignore
            )
        ),
      )
    | Delete =>
      ReasonReact.UpdateWithSideEffects(
        Deleted,
        (
          _self =>
            JSONRequest.jsonRequest(
              ~command="session_remove",
              ~arguments=
                Json.Encode.(
                  object_([("id", Json.Encode.int(session_id))])
                ),
              ~decoder=BaseResult.decode,
              (),
            )
            |> ignore
        ),
      )
    | Loaded(json) => ReasonReact.Update(Success(json))
    | LoadFailed => ReasonReact.Update(Failure)
    },
  render: self =>
    switch (self.state) {
    | NotAsked => ReasonReact.null
    | Deleted => ReasonReact.null
    | Loading => <tr> {ReasonReact.string("Loading...")} </tr>
    | Failure => <tr> {ReasonReact.string("Failed")} </tr>
    | Success(value) => render(self, value)
    },
  didMount: self => self.send(Load),
};