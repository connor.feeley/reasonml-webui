open BsReactstrap;

open CommonTypes;

[%raw {|require('Styles/Config.css')|}];

open DropdownOption;

type state = {
  systemInfo: option(SystemInfoResponse.t),
  tdmConfigs: option(list(TdmCfgResponse.t)),
  packetTimes: option(list(PacketTimeResponse.t)),
  channelFreqs: option(list(ChannelFreqResponse.t)),
};

type action =
  | LoadSystemInfo
  | LoadedSystemInfo(option(SystemInfoResponse.t))
  | LoadTdmCfgOptions
  | LoadedTdmCfgOptions(option(list(TdmCfgResponse.t)))
  | LoadAudioCfgOptions
  | LoadedAudioCfgOptions(
      option(list(ChannelFreqResponse.t)),
      option(list(PacketTimeResponse.t)),
    );

let component = ReasonReact.reducerComponent("SettingsPanel");

let make = _children => {
  ...component,
  initialState: () => {
    systemInfo: None,
    tdmConfigs: None,
    packetTimes: None,
    channelFreqs: None,
  },
  didMount: self => {
    self.send(LoadSystemInfo);
    self.send(LoadTdmCfgOptions);
    self.send(LoadAudioCfgOptions);
  },
  reducer: (action, state: state) =>
    switch (action) {
    | LoadSystemInfo =>
      ReasonReact.SideEffects(
        self =>
          Js.Promise.(
            JSONRequest.jsonRequest(
              ~command="get_system_info",
              ~decoder=GetSystemInfoResponse.decode,
              (),
            )
            |> Js.Promise.then_((result: option(GetSystemInfoResponse.t)) =>
                 switch (result) {
                 | Some(value) =>
                   let systemInfo = value.systemInfo;
                   resolve(self.send(LoadedSystemInfo(Some(systemInfo))));
                 }
               )
            |> ignore
          ),
      )
    | LoadedSystemInfo(info) =>
      ReasonReact.Update({...state, systemInfo: info})
    | LoadTdmCfgOptions =>
      ReasonReact.SideEffects(
        self =>
          Js.Promise.(
            JSONRequest.jsonRequest(
              ~command="get_tdm_cfg_options",
              ~decoder=GetTdmCfgResponse.decode,
              (),
            )
            |> Js.Promise.then_((result: option(GetTdmCfgResponse.t)) => {
                 let tdmConfigs =
                   result->Belt.Option.flatMap(value => value.list);
                 resolve(self.send(LoadedTdmCfgOptions(tdmConfigs)));
               })
            |> ignore
          ),
      )
    | LoadedTdmCfgOptions(config) =>
      ReasonReact.Update({...state, tdmConfigs: config})
    | LoadAudioCfgOptions =>
      ReasonReact.SideEffects(
        self =>
          Js.Promise.(
            JSONRequest.jsonRequest(
              ~command="get_audio_cfg_options",
              ~decoder=GetAudioCfgResponse.decode,
              (),
            )
            |> Js.Promise.then_((result: option(GetAudioCfgResponse.t)) => {
                 let channelFreqs =
                   result->Belt.Option.map(value => value.channel_freq);
                 let packetTimes =
                   result->Belt.Option.map(value => value.packet_time);
                 resolve(
                   self.send(
                     LoadedAudioCfgOptions(channelFreqs, packetTimes),
                   ),
                 );
               })
            |> ignore
          ),
      )
    | LoadedAudioCfgOptions(channelFreqs, packetTimes) =>
      ReasonReact.Update({...state, packetTimes, channelFreqs})
    },
  render: self => {
    <div id="config-panel">
      <div
        className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1> {ReasonReact.string("Config")} </h1>
      </div>
      {ReasonReact.string("Settings Panel")}
      <Card>
        <CardBody>
          <CardTitle> {ReasonReact.string("Settings")} </CardTitle>
          <CardText>
            <DropdownOption
              title="Channel Sampling Frequency"
              options={
                self.state.channelFreqs->Belt.Option.getWithDefault([])
                |> List.map((config: ChannelFreqResponse.t) =>
                     {
                       name: config.name,
                       value: string_of_int(config.value),
                       selected: false,
                     }
                   )
              }
            />
            <DropdownOption
              title="Packet Time"
              options={
                self.state.packetTimes->Belt.Option.getWithDefault([])
                |> List.map((config: PacketTimeResponse.t) =>
                     {
                       name: config.name,
                       value: string_of_int(config.value),
                       selected: false,
                     }
                   )
              }
            />
            <DropdownOption
              title="TDM Interface Mode"
              options={
                self.state.tdmConfigs->Belt.Option.getWithDefault([])
                |> List.map((config: TdmCfgResponse.t) =>
                     {
                       name: config.name,
                       value: string_of_int(config.ch_per_line),
                       selected: false,
                     }
                   )
              }
            />
          </CardText>
        </CardBody>
      </Card>
      <br />
      <Card>
        <CardBody>
          <CardTitle>
            {ReasonReact.string("Configuration Management")}
          </CardTitle>
          <CardText>
            <SubmitButton label="Save Running to Startup" />
            <SubmitButton label="Restore to Default Startup" />
            <SubmitButton label="Reset to Factory Defaults" />
          </CardText>
        </CardBody>
      </Card>
      <br />
      <Card>
        <CardBody>
          <CardTitle> {ReasonReact.string("Firmware")} </CardTitle>
          <CardText>
            <SubmitButton label="Reboot Device" />
            <SubmitButton label="Update Firmware" />
            {switch (self.state.systemInfo) {
             | Some(info) =>
               [
                 <TextInfo title="Product Name" value={info.product} />,
                 <TextInfo title="Firmware Version" value={info.fw_version} />,
                 <TextInfo title="Firmware Date" value={info.fw_date} />,
                 <br />,
                 <TextInfo title="ChipID Name" value={info.chipid_name} />,
                 <TextInfo
                   title="ChipID Version"
                   value={info.chipid_version}
                 />,
               ]
               ->Array.of_list
               ->ReasonReact.array
             | None => <div> {ReasonReact.string("Load Error")} </div>
             }}
          </CardText>
        </CardBody>
      </Card>
      <br />
      <Card>
        <CardBody>
          <CardTitle> {ReasonReact.string("NMOS Configuration")} </CardTitle>
          <CardText>
            {ReasonReact.string(
               "sysmgr_get_stored_parameters unimplemented: following is hardcoded",
             )}
            <DropdownOption
              title="Enable"
              options=[
                {name: "False", value: "false", selected: true},
                {name: "True", value: "true", selected: false},
              ]
            />
            <DropdownOption
              title="Advertisement Interface"
              options=[
                {name: "eth0", value: "eth0", selected: true},
                {name: "eth1", value: "eth1", selected: false},
                {name: "eth2", value: "eth2", selected: false},
              ]
            />
            <TextInput title="Device Name" value="Coveloz Board" />
            <TextInput title="Node Name" value="Coveloz Board" />
            <TextInput title="HTTP SDP Port" value="8081" />
            <TextInput title="Node Port" value="12345" />
            <TextInput title="Query Port" value="8235" />
            <TextInput title="Registration Port" value="8870" />
            <TextInput title="Shelf ID" value="0" />
            <TextInput title="Slot ID" value="0" />
          </CardText>
        </CardBody>
      </Card>
      <br />
      <Card>
        <CardBody>
          <CardTitle> {ReasonReact.string("SAP Configuration")} </CardTitle>
          <CardText>
            {ReasonReact.string("SAP unimplemented: following is hardcoded")}
            <DropdownOption
              title="Enable"
              options=[
                {name: "False", value: "false", selected: false},
                {name: "True", value: "true", selected: true},
              ]
            />
          </CardText>
        </CardBody>
      </Card>
      <br />
      /*
       <Card>
         <CardBody>
           <CardTitle> {ReasonReact.string("Logs")} </CardTitle>
           <CardText>
             {ReasonReact.string("SAP unimplemented: following is hardcoded")}
           </CardText>
         </CardBody>
       </Card>
       */
      <br />
      <Card>
        <CardBody>
          <CardTitle> {ReasonReact.string("IP Addresses")} </CardTitle>
          <CardText>
            <TableDisplay>
              <TableRow label="Name">
                ...{
                     ["IP", "MAC", "Link", "Mode"]
                     |> List.map(label => ReasonReact.string(label))
                     |> Array.of_list
                   }
              </TableRow>
              {switch (self.state.systemInfo) {
               | Some(info) =>
                 info.eth
                 |> List.map((interface: EthernetInterfaceInfo.t) =>
                      <TableRow label={interface.eth_name}>
                        ...{
                             [
                               interface.ipv4,
                               interface.mac,
                               interface.linkup ? "UP" : "DOWN",
                               "???",
                             ]
                             |> List.map(label => ReasonReact.string(label))
                             |> Array.of_list
                           }
                      </TableRow>
                    )
                 |> Array.of_list
                 |> ReasonReact.array
               | None => <div> {ReasonReact.string("Load Error")} </div>
               }}
            </TableDisplay>
          </CardText>
        </CardBody>
      </Card>
    </div>;
  },
};