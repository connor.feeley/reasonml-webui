const React = require('react');
const { Table, Column, Cell } = require('fixed-data-table-2');

class PureCell extends React.PureComponent {
    render() {
        return (
            <Cell />
        )
    }
}

class InterfaceCell extends React.PureComponent {
    render() {
        const { name, channels, ...props } = this.props;
        return (
            <Cell style={{ display: 'flex', flexDirection: 'row' }}>
                <div className={"interface-header-container " + (channels.filter(channel => channel.associated_stream).length > 0 ? "patch-grid-cell-header-inuse" : "")} >
                    <>
                        <div className="interface-header-name">{name.toUpperCase()}</div>
                        {/*<div className="interface-header-channel-count" style={{ height: '100%' }}>{channels.reduce((accumulator, channel) => channel.id + " " + accumulator, "")}</div>*/}
                        <div className="interface-header-channel-count" style={{ height: '100%' }}>{channels[channels.length - 1].id + "-" + channels[0].id}</div>
                    </>
                </div>
            </Cell>
        );
    }
}

class ActiveCell extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = { hover: false };
    };
    handleClick() {
        console.log("Row Index:", this.props.rowIndex, "Column Index:", this.props.columnIndex);
        this.props.parentClickListener(this.props.columnIndex, this.props.rowIndex);
    };
    render() {
        const activeClass = this.props.active ? "patch-grid-cell-connected" : "patch-grid-cell";
        const hoverClass = this.state.hover ? "patch-grid-cell-hovered" : "";
        return (
            <Cell onClick={() => this.handleClick()} onMouseEnter={() => this.setState({ hover: true })} onMouseLeave={() => this.setState({ hover: false })} className={activeClass + " " + hoverClass}>
            </Cell>
        );
    }
}

class PatchTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            myTableData: props.data
            /*[
                { name: 'Rylan' },
                { name: 'Amelia' },
                { name: 'Estevan' },
                { name: 'Florence' },
                { name: 'Tressa' },
            ]*/,
            interfaces: props.interfaces,
            streams: props.streams,
            parentClickListener: props.parentClickListener,
        };
    }

    componentWillReceiveProps(nextProps) {
        console.log('componentWillReceiveProps', nextProps);
        this.setState(nextProps);
    }

    render() {
        console.log(this.state.streams);
        console.log(this.state.interfaces);
        return (
            <Table
                /*bufferRowCount={128}*/
                isReorderable={false}
                isResizable={false}
                rowsCount={this.state.streams.length}
                rowHeight={30}
                headerHeight={150}
                width={1200}
                height={800}>
                <Column
                    header={<Cell>Streams</Cell>}
                    fixed={true}
                    pureRendering={true}
                    cell={props => (
                        <Cell className="stream-header" style={{}} {...props}>
                            {this.state.streams[props.rowIndex].stream_name}
                        </Cell>
                    )}
                    width={250}
                />
                {this.state.interfaces.map((iface, index) =>
                    < Column
                        pureRendering={true}
                        isResizable={false}
                        isReorderable={false}
                        allowCellsRecycling={true}
                        header={
                            < InterfaceCell name={iface.interface_name} channels={iface.channels} style={{ display: 'flex' }}>
                            </InterfaceCell>}
                        cell={() => (
                            //<ActiveCell {...props} active={iface.channels1.includes[props.rowIndex] % 2}></ActiveCell>
                            <PureCell />
                            /*
                            <ActiveCell parentClickListener={this.state.parentClickListener} columnIndex={index} {...props} active={iface.channels.filter(channel => channel.associated_stream && channel.associated_stream.remote_session_id === this.state.streams[props.rowIndex].remote_session_id).length > 0}>
                            </ActiveCell>
                            */
                        )
                        }
                        width={30}
                    />
                )
                }
            </Table >
        );
    }
}

export default PatchTable;