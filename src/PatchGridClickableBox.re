open PatchPanelCommon;

type state = {
  queued: bool,
  connected: bool,
};

type action =
  | Queue;

let component = ReasonReact.reducerComponent("PachGridClickableBox");

let make = (~parentSend, ~connected, ~x_index, ~y_index, _children) => {
  let patchEvent = (_event, _self) =>
    parentSend(HandlePatchEvent(x_index, y_index));
  {
    ...component,
    initialState: () => {queued: false, connected},
    reducer: (action, state) =>
      switch (action) {
      | Queue => ReasonReact.Update({...state, queued: !state.queued})
      },
    render: self => {
      let onClick(event, self) = {
        parentSend(HandlePatchEvent(x_index, y_index));
        self.ReasonReact.send(Queue);
      };

      let className =
        self.state.connected ?
          "patch-grid-cell patch-grid-cell-clickable patch-grid-cell-connected" :
          self.state.queued ? "patch-grid-cell patch-grid-cell-clickable patch-grid-cell-queued" :
          "patch-grid-cell patch-grid-cell-clickable";

      <div onClick={self.handle(onClick)} className />;
    },
  };
};