open CommonTypes;

let component = ReasonReact.statelessComponent("DestinationPanel");

let make = _children => {
  ...component,
  render: _self =>
    <div>
      <div
        className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1> {ReasonReact.string("Destination")} </h1>
      </div>
      <div className="row wrapper wrapper-content animated fadeInRight">
        <InfoPane title="Destinations">
          <table className="table table-hover">
            <tbody>
              <tr>
                ...{
                     [
                       "Name",
                       "Status",
                       "On/Off",
                       "#Ch",
                       "Audio Format",
                       "Link Offset (us)",
                       "Packet Time(us)",
                       "Delete",
                     ]
                     |> List.map(col => <th> {ReasonReact.string(col)} </th>)
                     |> Array.of_list
                   }
              </tr>
              <RequestGetSessionList
                session_type=Destination
                render={
                  session_list =>
                    session_list
                    |> List.map(session =>
                         <RequestGetSessionBasicInfo
                           session_type=Destination
                           session_id=SessionDescription.(session.session)
                           render={
                             (component, session) =>
                               <tr>
                                 <td> {ReasonReact.string(session.name)} </td>
                                 <td> {ReasonReact.string("???")} </td>
                                 <td>
                                   {
                                     ReasonReact.string(
                                       List.hd(session.session_list).state,
                                     )
                                   }
                                 </td>
                                 <td>
                                   {
                                     ReasonReact.string(
                                       session.session_list
                                       |> List.length
                                       |> string_of_int,
                                     )
                                   }
                                 </td>
                                 <td>
                                   {
                                     ReasonReact.string(
                                       List.hd(session.session_list).codec,
                                     )
                                   }
                                 </td>
                                 <td>
                                   {
                                     ReasonReact.string(
                                       List.hd(session.session_list).
                                         link_offset
                                       |> string_of_int,
                                     )
                                   }
                                 </td>
                                 <td>
                                   {
                                     ReasonReact.string(
                                       List.hd(session.session_list).ptime
                                       |> string_of_int,
                                     )
                                   }
                                 </td>
                                 <td>
                                   <a
                                     className="btn btn-outline-primary"
                                     onClick={
                                       _event =>
                                         component.send(
                                           RequestGetSessionBasicInfo.Delete,
                                         )
                                     }>
                                     {ReasonReact.string("Delete")}
                                   </a>
                                 </td>
                               </tr>
                           }
                         />
                       )
                    |> Array.of_list
                    |> ReasonReact.array
                }
              />
            </tbody>
          </table>
        </InfoPane>
      </div>
    </div>,
};