module ClockInfo = {
  type t = {
    clockAccuracy: int,
    clockAccuracyTable6: string,
    clockClass: int,
    id: string,
    priority1: int,
    priority2: int,
    scaledLogVariance: int,
    slaveOnly: bool,
  };
  let decode = json =>
    Json.Decode.{
      clockAccuracy: json |> field("clockAccuracy", int),
      clockAccuracyTable6: json |> field("clockAccuracyTable6", string),
      clockClass: json |> field("clockClass", int),
      id: json |> field("id", string),
      priority1: json |> field("priority1", int),
      priority2: json |> field("priority2", int),
      scaledLogVariance: json |> field("scaledLogVariance", int),
      slaveOnly: json |> field("slaveOnly", bool),
    };
  let render = (value: t, title: string) =>
    <TableDisplay>
      <u> <TableRow label=title /> </u>
      <TableRow label="Clock Accuracy">
        {ReasonReact.string(string_of_int(value.clockAccuracy))}
      </TableRow>
      <TableRow label="Clock Class">
        {ReasonReact.string(string_of_int(value.clockClass))}
      </TableRow>
      <TableRow label="ID"> {ReasonReact.string(value.id)} </TableRow>
      <TableRow label="Priority 1">
        {ReasonReact.string(string_of_int(value.priority1))}
      </TableRow>
    </TableDisplay>;
};
module PortInfo = {
  type t = {
    announceInterval: int,
    announceReceiptTimeout: int,
    delayMechanism: string,
    id: string,
    logMinDelayReqInterval: int,
    logMinPdelayReqInterval: int,
    peerMeanPathDelay: int,
    portId: int,
    roleStatus: string,
    roleStatusId: int,
    syncInterval: int,
  };
  let decode = json =>
    Json.Decode.{
      announceInterval: json |> field("announceInterval", int),
      announceReceiptTimeout: json |> field("announceReceiptTimeout", int),
      delayMechanism: json |> field("delayMechanism", string),
      id: json |> field("id", string),
      logMinDelayReqInterval: json |> field("logMinDelayReqInterval", int),
      logMinPdelayReqInterval: json |> field("logMinPdelayReqInterval", int),
      peerMeanPathDelay: json |> field("peerMeanPathDelay", int),
      portId: json |> field("portId", int),
      roleStatus: json |> field("roleStatus", string),
      roleStatusId: json |> field("roleStatusId", int),
      syncInterval: json |> field("syncInterval", int),
    };
};
module PtpGet = {
  type t = {
    auxmsg: option(string),
    command: string,
    error: int,
    error_string: string,
    success: bool,
    grandMaster: ClockInfo.t,
    local: ClockInfo.t,
    meanPathDelay: int,
    offsetFromMaster: int,
    offsetScaledLogVariance: int,
    port: list(PortInfo.t),
    profile: string,
    slaveOnly: bool,
    stepsRemoved: int,
  };
  let decode = json =>
    Json.Decode.{
      auxmsg: json |> field("auxmsg", optional(string)),
      command: json |> field("command", string),
      error: json |> field("error", int),
      error_string: json |> field("error_string", string),
      success: json |> field("success", bool),
      grandMaster: json |> field("grandMaster", ClockInfo.decode),
      local: json |> field("local", ClockInfo.decode),
      meanPathDelay: json |> field("meanPathDelay", int),
      offsetFromMaster: json |> field("offsetFromMaster", int),
      offsetScaledLogVariance: json |> field("offsetScaledLogVariance", int),
      port: json |> field("port", list(PortInfo.decode)),
      profile: json |> field("profile", string),
      slaveOnly: json |> field("slaveOnly", bool),
      stepsRemoved: json |> field("stepsRemoved", int),
    };
};

type action('a) =
  | Load
  | Loaded('a)
  | LoadFailed;

type state =
  | NotAsked
  | Loading
  | Failure
  | Success(PtpGet.t);

let component = ReasonReact.reducerComponent("JSONRequest");

let make = _children => {
  ...component,
  initialState: () => NotAsked,
  didMount: self => self.send(Load),
  reducer: (action, _state) =>
    switch (action) {
    | Load =>
      ReasonReact.UpdateWithSideEffects(
        Loading,
        self =>
          Js.Promise.(
            JSONRequest.jsonRequest(
              ~command="ptp_get",
              ~decoder=PtpGet.decode,
              (),
            )
            |> Js.Promise.then_(result =>
                 switch (result) {
                 | Some(value) => resolve(self.send(Loaded(value)))
                 | None => resolve(self.send(LoadFailed))
                 }
               )
            |> ignore
          ),
      )
    | Loaded(json) => ReasonReact.Update(Success(json))
    | LoadFailed => ReasonReact.Update(Failure)
    },
  render: self =>
    switch (self.state) {
    | NotAsked =>
      <div>
        <a
          className="btn btn-outline-primary"
          onClick={_event => self.send(Load)}>
          {ReasonReact.string("Load")}
        </a>
      </div>
    | Loading => <div> {ReasonReact.string("Loading...")} </div>
    | Failure => <div> {ReasonReact.string("Failed")} </div>
    | Success(value) =>
      <div>
        <TableDisplay>
          <TableRow label="Grandmaster ID">
            {ReasonReact.string(value.grandMaster.id)}
          </TableRow>
          <TableRow label="Grandmaster Priority1">
            {ReasonReact.string(string_of_int(value.grandMaster.priority1))}
          </TableRow>
          <TableRow label="Offset from Master">
            {ReasonReact.string(string_of_int(value.offsetFromMaster))}
          </TableRow>
          <TableRow label="Mean Path Delay">
            {ReasonReact.string(string_of_int(value.meanPathDelay))}
          </TableRow>
          <TableRow label="Clock Accuracy">
            {ReasonReact.string(
               string_of_int(value.grandMaster.clockAccuracy)
               ++ "("
               ++ value.grandMaster.clockAccuracyTable6
               ++ ")",
             )}
          </TableRow>
        </TableDisplay>
      </div>
    },
};