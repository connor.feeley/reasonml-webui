open PatchPanelCommon;

let component = ReasonReact.statelessComponent("PachGridStreamHeader");

let make = (~stream_name, ~number_channels, ~connected, _children) => {
  {
    ...component,
    render: self => {
      let className =
        connected ?
          "stream-header stream-header-name patch-grid-cell-header-inuse" :
          "stream-header stream-header-name";
      <div
        className="stream-header-container"
        style={ReactDOMRe.Style.make(~width="400px", ())}>
        <div className> {ReasonReact.string(stream_name)} </div>
        <div className="stream-header stream-header-channel-count">
          {ReasonReact.string(string_of_int(number_channels) ++ " Channels")}
        </div>
      </div>;
    },
  };
};