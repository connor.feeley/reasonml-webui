open PatchPanelCommon;

let component = ReasonReact.statelessComponent("PatchPanelQueue");

let make = (~actions, _children) => {
  ...component,
  render: _self => {
    <div id="patch-queue">
      <h5 style={ReactDOMRe.Style.make(~textAlign="center", ())}>
        {ReasonReact.string("Queued Actions")}
      </h5>
      {actions->List.length == 0 ?
         {<div style={ReactDOMRe.Style.make(~textAlign="center", ())}>
            {ReasonReact.string("-")}
          </div>} :
         {[
            <div id="patch-queue-list">
              {actions->List.rev
               |> List.mapi((index, action) =>
                    <div>
                      <b>
                        {ReasonReact.string(string_of_int(index + 1) ++ ") ")}
                      </b>
                      //{ReasonReact.string(action)}
                      {ReasonReact.string(
                         action.stream.stream_name
                         ++ {js| → |js}
                         ++ action.interface.interface_name
                         ++ " ["
                         ++ channelsAsString(action.interface.channels)
                         ++ "]",
                       )}
                    </div>
                  )
               |> Array.of_list
               |> ReasonReact.array}
            </div>,
            <div id="patch-queue-footer">
              <button
                style={ReactDOMRe.Style.make(
                  ~backgroundColor="#282828",
                  ~color="white",
                  ~padding="1em 1.5em",
                  ~textDecoration="none",
                  ~textTransform="uppercase",
                  (),
                )}>
                {ReasonReact.string("Apply")}
              </button>
            </div>,
          ]
          ->Array.of_list
          ->ReasonReact.array}}
    </div>;
  },
};