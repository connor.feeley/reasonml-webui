let component = ReasonReact.statelessComponent("TestCell");
let make = (~rowIndex, _) => {
  ...component,
  render: _ => <div className="patch-grid-cell patch-grid-cell-clickable" />,
};