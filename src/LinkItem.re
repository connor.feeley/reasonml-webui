open BsReactstrap;

let component = ReasonReact.statelessComponent("LinkItem");

let handleClick = (href, event, _self) =>
  if (!ReactEvent.Mouse.defaultPrevented(event)) {
    ReactEvent.Mouse.preventDefault(event);
    ReasonReact.Router.push(href);
  };

let make = (~name: string, ~href: string, _children) => {
  ...component,
  render: _self =>
    <NavItem>
      <NavLink
        href onClick={handleClick(href)} className="text-dark" active=true>
        {ReasonReact.string(name)}
      </NavLink>
    </NavItem>,
};