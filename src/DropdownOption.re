type dropdownOption = {
  name: string,
  value: string,
  selected: bool,
};

type state = {
  unset: bool, /* if the parameter has been changed but not updated */
  newValue: option(int),
};

type action =
  | ChangeChoice(int)
  | Apply;

let component = ReasonReact.reducerComponent("DropbownOption");

let make = (~title, ~options, _children) => {
  ...component,
  initialState: () => {unset: false, newValue: None},
  reducer: (action, state) =>
    switch (action) {
    | ChangeChoice(value) =>
      ReasonReact.Update({unset: true, newValue: Some(value)})
    | Apply => ReasonReact.Update({...state, unset: false})
    },
  render: self => {
    <div>
      <div> <b> {ReasonReact.string(title)} </b> </div>
      <div>
        <select
          className="config-select"
          onChange={event =>
            self.send(
              ChangeChoice(
                int_of_string(event->ReactEvent.Form.target##value),
              ),
            )
          }>
          {{options
            |> List.map(option =>
                 <option value={option.value} selected={option.selected}>
                   {ReasonReact.string(option.name)}
                 </option>
               )}
           ->Array.of_list
           ->ReasonReact.array}
        </select>
        {self.state.unset ?
           {<span style={ReactDOMRe.Style.make(~paddingLeft="1em", ())}>
              <button
                onClick={_event => self.send(Apply)}
                style={ReactDOMRe.Style.make(
                  ~backgroundColor="#orange",
                  ~color="black",
                  ~textDecoration="none",
                  ~textTransform="uppercase",
                  ~boxSizing="border-box",
                  ~borderRadius="0.5em",
                  ~lineHeight="100%",
                  (),
                )}>
                {ReasonReact.string("Apply")}
              </button>
            </span>} :
           <span />}
      </div>
    </div>;
  },
};