type session_type =
  | Source
  | Destination;
module HostDescription = {
  type t = {
    address: string,
    ip: string,
  };
  let decode = json =>
    Json.Decode.{
      address: json |> field("address", string),
      ip: json |> field("ip", string),
    };
};
module TimeDescription = {
  type t = {
    start: int,
    stop: int,
  };
  let decode = json =>
    Json.Decode.{
      start: json |> field("start", int),
      stop: json |> field("stop", int),
    };
};
module PtpDescription = {
  type t = {
    domain: int,
    gmid: string,
  };
  let decode = json =>
    Json.Decode.{
      domain: json |> field("domain", int),
      gmid: json |> field("gmid", string),
    };
};

module RtpMapDescription = {
  type t = {
    channel_freq: int,
    codec: string,
    number_channels: int,
    payload_type: int,
  };
  let decode = json =>
    Json.Decode.{
      channel_freq: json |> field("channel-freq", int),
      codec: json |> field("codec", string),
      number_channels: json |> field("number-channels", int),
      payload_type: json |> field("payload-type", int),
    };
};
module MediaDescription = {
  type t = {
    clock_is_ptp: bool,
    group: string,
    info: string,
    local_ip: string,
    mediaclk: int,
    number_samples: int,
    protocol: string,
    ptime: float,
    ptp: PtpDescription.t,
    rtpmap: list(RtpMapDescription.t),
    transport_ip: string,
    ttl: int,
    media_type: string,
    udp: int,
  };
  let decode = json =>
    Json.Decode.{
      clock_is_ptp: json |> field("clock-is-ptp", bool),
      group: json |> field("group", string),
      info: json |> field("info", string),
      local_ip: json |> field("local-ip", string),
      mediaclk: json |> field("mediaclk", int),
      number_samples: json |> field("number-samples", int),
      protocol: json |> field("protocol", string),
      ptime: json |> field("ptime", float),
      ptp: json |> field("ptp", PtpDescription.decode),
      rtpmap: json |> field("rtpmap", list(RtpMapDescription.decode)),
      transport_ip: json |> field("transport-ip", string),
      ttl: json |> field("ttl", int),
      media_type: json |> field("type", string),
      udp: json |> field("udp", int),
    };
};
module AdvertisedSessionDescription = {
  type t = {
    id: int,
    info: string,
    local_ip: string,
    media: list(MediaDescription.t),
    protocol_version: int,
    time: TimeDescription.t,
    user_name: string,
    version: int,
  };
  let decode = json =>
    Json.Decode.{
      id: json |> field("id", int),
      info: json |> field("info", string),
      local_ip: json |> field("local-ip", string),
      media: json |> field("media", list(MediaDescription.decode)),
      protocol_version: json |> field("protocol-version", int),
      time: json |> field("time", TimeDescription.decode),
      user_name: json |> field("user-name", string),
      version: json |> field("version", int),
    };
};
module EndpointDescription = {
  type t = {
    host: HostDescription.t,
    inuse: bool,
    sdp_raw: string,
    session: AdvertisedSessionDescription.t,
    session_id: int,
    session_id_str: string,
    status: string,
    url: string,
  };
  let decode = json =>
    Json.Decode.{
      host: json |> field("host", HostDescription.decode),
      inuse: json |> field("inuse", bool),
      sdp_raw: json |> field("sdp-raw", string),
      session: json |> field("session", AdvertisedSessionDescription.decode),
      session_id: json |> field("session-id", int),
      session_id_str: json |> field("session-id-str", string),
      status: json |> field("status", string),
      url: json |> field("url", string),
    };
};
module GetAdvertisedSessionList = {
  type t = {
    auxmsg: option(string),
    command: string,
    error: int,
    error_string: string,
    success: bool,
    endpoint_list: option(list(EndpointDescription.t)),
  };
  let decode = json =>
    Json.Decode.{
      auxmsg: json |> field("auxmsg", optional(string)),
      command: json |> field("command", string),
      error: json |> field("error", int),
      error_string: json |> field("error_string", string),
      success: json |> field("success", bool),
      endpoint_list:
        json |> field("list", optional(list(EndpointDescription.decode))),
    };
};
module BaseResult = {
  type t = {
    auxmsg: option(string),
    command: string,
    error: int,
    error_string: string,
    success: bool,
  };
  let decode = json =>
    Json.Decode.{
      auxmsg: json |> field("auxmsg", optional(string)),
      command: json |> field("command", string),
      error: json |> field("error", int),
      error_string: json |> field("error_string", string),
      success: json |> field("success", bool),
    };
};
module ChannelDescription = {
  type t = {
    id: int,
    position: int,
    channel_type: string,
  };
  let decode = json =>
    Json.Decode.{
      id: json |> field("id", int),
      position: json |> field("position", int),
      channel_type: json |> field("type", string),
    };
};
module SessionBasicDescription = {
  type t = {
    channel_freq: int,
    channel_list: list(ChannelDescription.t),
    session_class: string,
    codec: string,
    l1_bandwidth: int,
    link_offset: int,
    media_clk_offset: int,
    name: string,
    packet_size: int,
    ptime: int,
    state: string,
    session_type: string,
  };
  let decode = json =>
    Json.Decode.{
      channel_freq: json |> field("channel-freq", int),
      channel_list:
        json |> field("channel-list", list(ChannelDescription.decode)),
      session_class: json |> field("class", string),
      codec: json |> field("codec", string),
      l1_bandwidth: json |> field("l1-bandwidth", int),
      link_offset: json |> field("link-offset", int),
      media_clk_offset: json |> field("media-clk-offset", int),
      name: json |> field("name", string),
      packet_size: json |> field("packet-size", int),
      ptime: json |> field("ptime", int),
      state: json |> field("state", string),
      session_type: json |> field("type", string),
    };
};
module GetSessionBasicInfo = {
  type t = {
    auxmsg: option(string),
    command: string,
    error: int,
    error_string: string,
    success: bool,
    id: int,
    name: string,
    ptp_domain: int,
    ptp_gmid: string,
    remote_sid: int,
    remote_sid_str: string,
    rtsp_url: string,
    session_list: list(SessionBasicDescription.t),
  };
  let decode = json =>
    Json.Decode.{
      auxmsg: json |> field("auxmsg", optional(string)),
      command: json |> field("command", string),
      error: json |> field("error", int),
      error_string: json |> field("error_string", string),
      success: json |> field("success", bool),
      id: json |> field("id", int),
      name: json |> field("name", string),
      ptp_domain: json |> field("ptp-domain", int),
      ptp_gmid: json |> field("ptp-gmid", string),
      remote_sid: json |> field("remote-sid", int),
      remote_sid_str: json |> field("remote-sid-str", string),
      rtsp_url: json |> field("rtsp-url", string),
      session_list:
        json |> field("session-list", list(SessionBasicDescription.decode)),
    };
};
module SessionDescription = {
  type t = {
    name: string,
    redundant_type: string,
    remote_sid: int,
    remote_sid_str: string,
    session: int,
    state: string,
    stream: int,
    stream_hw_id: int,
    stream_name: string,
    stream_type: string,
  };
  let decode = json =>
    Json.Decode.{
      name: json |> field("name", string),
      redundant_type: json |> field("redundant-type", string),
      remote_sid: json |> field("remote-sid", int),
      remote_sid_str: json |> field("remote-sid-str", string),
      session: json |> field("session", int),
      state: json |> field("state", string),
      stream: json |> field("stream", int),
      stream_hw_id: json |> field("stream-hw-id", int),
      stream_name: json |> field("stream-name", string),
      stream_type: json |> field("type", string),
    };
};
module GetSessionList = {
  type t = {
    auxmsg: option(string),
    command: string,
    error: int,
    error_string: string,
    success: bool,
    list: option(list(SessionDescription.t)),
  };
  let decode = json =>
    Json.Decode.{
      auxmsg: json |> field("auxmsg", optional(string)),
      command: json |> field("command", string),
      error: json |> field("error", int),
      error_string: json |> field("error_string", string),
      success: json |> field("success", bool),
      list:
        json |> field("list", optional(list(SessionDescription.decode))),
    };
};
module CapabilityDescription = {
  type t = {
    capability: int,
    aux_string: string,
  };
  let decode = json =>
    Json.Decode.{
      capability: json |> field("capability", int),
      aux_string: json |> field("string", string),
    };
};
module InputOutputDescription = {
  type t = {
    name: string,
    redundant_type: string,
    remote_sid: int,
    remote_sid_str: string,
    session: int,
    state: string,
    stream: int,
    stream_hw_id: int,
    stream_name: string,
    stream_type: string,
  };
  let decode = json =>
    Json.Decode.{
      name: json |> field("name", string),
      redundant_type: json |> field("redundant-type", string),
      remote_sid: json |> field("remote-sid", int),
      remote_sid_str: json |> field("remote-sid-str", string),
      session: json |> field("session", int),
      state: json |> field("state", string),
      stream: json |> field("stream", int),
      stream_hw_id: json |> field("stream-hw-id", int),
      stream_name: json |> field("stream-name", string),
      stream_type: json |> field("type", string),
    };
};
module MediaTypeDescription = {
  type t = {
    media_type: int,
    aux_string: string,
  };
  let decode = json =>
    Json.Decode.{
      media_type: json |> field("media-type", int),
      aux_string: json |> field("string", string),
    };
};
module ChannelSessionDescription = {
  type t = {
    blocked: bool,
    capabilities: CapabilityDescription.t,
    id: int,
    input: InputOutputDescription.t,
    media_type: MediaTypeDescription.t,
    name: string,
    name_input: string,
    name_output: string,
    output: InputOutputDescription.t,
  };
  let decode = json =>
    Json.Decode.{
      blocked: json |> field("blocked", bool),
      capabilities:
        json |> field("capabilities", CapabilityDescription.decode),
      id: json |> field("id", int),
      input: json |> field("input", InputOutputDescription.decode),
      media_type: json |> field("media-type", MediaTypeDescription.decode),
      name: json |> field("name", string),
      name_input: json |> field("name-input", string),
      name_output: json |> field("name-output", string),
      output: json |> field("output", InputOutputDescription.decode),
    };
};
module ChannelsList = {
  type t = {
    channels: list(ChannelSessionDescription.t),
    aux_string: string,
    aux_type: int,
  };
  let decode = json =>
    Json.Decode.{
      channels:
        json |> field("channels", list(ChannelSessionDescription.decode)),
      aux_string: json |> field("string", string),
      aux_type: json |> field("type", int),
    };
};
module GetChannelUsageTable = {
  type t = {
    auxmsg: option(string),
    command: string,
    error: int,
    error_string: string,
    success: bool,
    list: list(ChannelsList.t),
  };
  let decode = json =>
    Json.Decode.{
      auxmsg: json |> field("auxmsg", optional(string)),
      command: json |> field("command", string),
      error: json |> field("error", int),
      error_string: json |> field("error_string", string),
      success: json |> field("success", bool),
      list: json |> field("list", list(ChannelsList.decode)),
    };
};

module SfpInfo = {
  type t = {
    part_number: string,
    present: bool,
    rx_power: int,
    temperature: int,
    tx_power: int,
    wavelength: int,
  };
  let decode = json =>
    Json.Decode.{
      part_number: json |> field("part_number", string),
      present: json |> field("present", bool),
      rx_power: json |> field("rx_power", int),
      temperature: json |> field("temperature", int),
      tx_power: json |> field("tx_power", int),
      wavelength: json |> field("wavelength", int),
    };
};

module EthernetInterfaceInfo = {
  type t = {
    eth_name: string,
    ifid: int,
    ipv4: string,
    linkup: bool,
    mac: string,
    sfp: SfpInfo.t,
  };
  let decode = json =>
    Json.Decode.{
      eth_name: json |> field("eth-name", string),
      ifid: json |> field("ifid", int),
      ipv4: json |> field("ipv4", string),
      linkup: json |> field("linkup", bool),
      mac: json |> field("mac", string),
      sfp: json |> field("sfp", SfpInfo.decode),
    };
};

module SystemInfoResponse = {
  type t = {
    /*advertisement: option(string),*/
    bitstream: string,
    chipid_name: string,
    chipid_version: string,
    /*device_capabilities: option(string),*/
    eth: list(EthernetInterfaceInfo.t),
    fan_speed: int,
    fw_date: string,
    fw_version: string,
    madi_lock: option(bool),
    product: string,
    serial_number: string,
  };
  let decode = json =>
    Json.Decode.{
      bitstream: json |> field("bitstream", string),
      chipid_name: json |> field("chipid-name", string),
      chipid_version: json |> field("chipid-version", string),
      eth: json |> field("eth", list(EthernetInterfaceInfo.decode)),
      fan_speed: json |> field("fan-speed", Json.Decode.field("main", int)),
      fw_date: json |> field("fw-date", string),
      fw_version: json |> field("fw-version", string),
      madi_lock: json |> optional(field("madi-lock", bool)),
      product: json |> field("product", string),
      serial_number: json |> field("serial-number", string),
    };
};

/* get_system_info response */
module GetSystemInfoResponse = {
  type t = {
    auxmsg: option(string),
    command: string,
    error: int,
    error_string: string,
    success: bool,
    systemInfo: SystemInfoResponse.t,
  };
  let decode = json =>
    Json.Decode.{
      auxmsg: json |> field("auxmsg", optional(string)),
      command: json |> field("command", string),
      error: json |> field("error", int),
      error_string: json |> field("error_string", string),
      success: json |> field("success", bool),
      systemInfo: json |> field("systemInfo", SystemInfoResponse.decode),
    };
};

module TdmCfgResponse = {
  type t = {
    ch_per_line: int,
    name: string,
  };
  let decode = json =>
    Json.Decode.{
      ch_per_line: json |> field("ch_per_line", int),
      name: json |> field("name", string),
    };
};

/* get_tdm_cfg_options response */
module GetTdmCfgResponse = {
  type t = {
    auxmsg: option(string),
    command: string,
    error: int,
    error_string: string,
    success: bool,
    list: option(list(TdmCfgResponse.t)),
  };
  let decode = json =>
    Json.Decode.{
      auxmsg: json |> field("auxmsg", optional(string)),
      command: json |> field("command", string),
      error: json |> field("error", int),
      error_string: json |> field("error_string", string),
      success: json |> field("success", bool),
      list: json |> field("list", optional(list(TdmCfgResponse.decode))),
    };
};

module ChannelFreqResponse = {
  type t = {
    name: string,
    value: int,
  };
  let decode = json =>
    Json.Decode.{
      name: json |> field("string", string),
      value: json |> field("value", int),
    };
};

module PacketTimeResponse = {
  type t = {
    name: string,
    value: int,
  };
  let decode = json =>
    Json.Decode.{
      name: json |> field("string", string),
      value: json |> field("value", int),
    };
};

/* get_tdm_cfg_options response */
module GetAudioCfgResponse = {
  type t = {
    auxmsg: option(string),
    command: string,
    error: int,
    error_string: string,
    success: bool,
    channel_freq: list(ChannelFreqResponse.t),
    packet_time: list(PacketTimeResponse.t),
  };
  let decode = json =>
    Json.Decode.{
      auxmsg: json |> field("auxmsg", optional(string)),
      command: json |> field("command", string),
      error: json |> field("error", int),
      error_string: json |> field("error_string", string),
      success: json |> field("success", bool),
      channel_freq:
        json |> field("channel-freq", list(ChannelFreqResponse.decode)),
      packet_time: json |> field("ptime", list(PacketTimeResponse.decode)),
    };
};