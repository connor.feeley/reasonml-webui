open PatchPanelCommon;

let rec repeatElement = (list, n, element) =>
  switch (n) {
  | 0 => list
  | _ => [element, ...list]->repeatElement(n - 1, element)
  };

type state = {streamListDivRef: ref(option(Dom.element))};

let component = ReasonReact.reducerComponent("PatchGridFixed");

let make =
    (
      ~parentSend,
      ~networkStreamList: array(networkStreamDescription),
      ~interfaces: array(interfaceDescription),
      ~streamSize: int,
      ~dimension_x: int,
      ~dimension_y: int,
      ~verticalScrollRef: ref(option(Dom.element)),
      ~horizontalScrollRef: ref(option(Dom.element)),
      _children,
    ) => {
  let handleStreamListScroll = (event, self) => (); /* TODO: unused */
  {
    ...component,
    initialState: () => {streamListDivRef: ref(None)},
    shouldUpdate: oldNewSelf => oldNewSelf.oldSelf == oldNewSelf.newSelf,
    reducer: ((), _state: state) => ReasonReact.NoUpdate,
    render: self => {
      /* Array of booleans on whether or not a stream is connected to _any_ local interface */
      let isInterfaceConnectedToStream = (interface_index, stream_index) => {
        /* Return true if the interface index is connected to the stream index */
        let interface = interfaces[interface_index];
        let stream = networkStreamList[stream_index];
        let connectedChannels =
          interface.channels
          |> List.filter(channel =>
               switch (channel.associated_stream) {
               | Some(associated_stream) =>
                 associated_stream.remote_session_id
                 == stream.remote_session_id
               | None => false
               }
             );
        if (connectedChannels->List.length > 0) {
          Js.log(interface);
          Js.log(stream);
          true;
        } else {
          false;
        };
      };
      let isStreamConnectedMap =
        networkStreamList
        |> Array.mapi((index_stream, _) =>
             interfaces
             |> Array.mapi((index_interface, _) =>
                  isInterfaceConnectedToStream(index_interface, index_stream)
                )
             |> Array.to_list
             |> List.filter(elem => elem == true)
           )
        |> Array.map(connected_list => connected_list->List.length > 0);
      Js.log(isStreamConnectedMap);
      <div
        id="grid-holder"
        style={ReactDOMRe.Style.make(
          ~gridTemplateColumns=
            "400px " ++ string_of_int(interfaces->Array.length * 20) ++ "px",
          ~gridTemplateRows=
            "150px "
            ++ string_of_int(networkStreamList->Array.length * 20)
            ++ "px",
          (),
        )}>
        <div id="grid-corner-blocker" />
        <div id="interfaces-list">
          {interfaces
           |> Array.map(interface =>
                <PatchGridInterfaceHeader
                  interface_name={interface.interface_name}
                  channel_number_low={interface.channels->List.rev->List.hd.id}
                  channel_number_high={interface.channels->List.hd.id}
                  connected={
                    interface.channels
                    |> List.filter(channel =>
                         channel.associated_stream->Belt.Option.isSome
                       )
                    |> List.length > 0
                  }
                />
              )
           |> ReasonReact.array}
        </div>
        <div id="streams-list">
          {networkStreamList
           |> Array.mapi((index, stream) =>
                <PatchGridStreamHeader
                  stream_name={stream.stream_name}
                  number_channels={stream.number_channels_stream}
                  connected={Array.get(isStreamConnectedMap, index)}
                />
              )
           |> ReasonReact.array}
        </div>
        <div
          id="grid"
          style={ReactDOMRe.Style.make(
            ~gridTemplateColumns=
              "repeat("
              ++ string_of_int(interfaces->Array.length)
              ++ ", 20px)",
            ~gridTemplateRows=
              "repeat(" ++ string_of_int(dimension_y) ++ ", 20px)",
            (),
          )}>
          {Array.init(dimension_y, y_index =>
             Array.init(interfaces->Array.length, x_index =>
               <PatchGridClickableBox
                 connected={isInterfaceConnectedToStream(x_index, y_index)}
                 parentSend
                 x_index
                 y_index
               />
             )
             |> ReasonReact.array
           )
           |> ReasonReact.array}
        </div>
      </div>;
    },
  };
};