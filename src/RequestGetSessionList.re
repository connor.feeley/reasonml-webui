open CommonTypes;

type action('a) =
  | Load
  | Loaded('a)
  | LoadFailed;

type state =
  | NotAsked
  | Loading
  | Failure
  | Success(GetSessionList.t);

let component = ReasonReact.reducerComponent("GetSessionList");

let make = (~session_type: session_type, ~render, _children) => {
  ...component,
  initialState: () => NotAsked,
  reducer: (action, _state) =>
    switch (action) {
    | Load =>
      ReasonReact.UpdateWithSideEffects(
        Loading,
        (
          self =>
            Js.Promise.(
              JSONRequest.jsonRequest(
                ~command=
                  switch (session_type) {
                  | Source => "get_source_session_list"
                  | Destination => "get_destination_session_list"
                  },
                /*~arguments=Json.Encode.int(10),*/
                ~decoder=GetSessionList.decode,
                (),
              )
              |> Js.Promise.then_(result =>
                   switch (result) {
                   /* If we successfully got a value, request detailed information for each session */
                   | Some(value) =>
                     switch (GetSessionList.(value.list)) {
                     | Some(session_list) =>
                       session_list
                       |> List.map(session =>
                            Js.log(SessionDescription.(session.session))
                          );
                       resolve(self.send(Loaded(value)));
                     | None => resolve(self.send(Loaded(value)))
                     }
                   | None => resolve(self.send(LoadFailed))
                   }
                 )
              |> ignore
            )
        ),
      )
    | Loaded(json) => ReasonReact.Update(Success(json))
    | LoadFailed => ReasonReact.Update(Failure)
    },
  render: self =>
    switch (self.state) {
    | NotAsked =>
      <div>
        <a
          className="btn btn-outline-primary"
          onClick=(_event => self.send(Load))>
          {ReasonReact.string("Load")}
        </a>
      </div>
    | Loading => <div> {ReasonReact.string("Loading...")} </div>
    | Failure => <div> {ReasonReact.string("Failed")} </div>
    | Success(value) =>
      switch (value.list) {
      | Some(session_list) => render(session_list)
      | None => ReasonReact.null
      }
    },
  didMount: self => self.send(Load),
};