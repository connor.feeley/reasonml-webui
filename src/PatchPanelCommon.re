/* TYPES */
type networkStreamDescription = {
  stream_name: string,
  number_channels_stream: int,
  remote_session_id: int,
  local_session_id: int,
};

type channelDescription = {
  interface_name: string,
  id: int,
  associated_stream: option(networkStreamDescription),
};

type interfaceDescription = {
  interface_name: string,
  channels: list(channelDescription),
};

type interfaceArray = array(interfaceDescription);
type streamArray = array(networkStreamDescription);

type patchActionQueueItem = {
  stream: networkStreamDescription, /* network stream description */
  interface: interfaceDescription /* interface and channels to map to */
};

/* STATE/ACTION */
type state = {
  channels_model: option(array(channelDescription)),
  interfaces: option(array(interfaceDescription)),
  networkStreams: option(array(networkStreamDescription)),
  interfaceGroupSize: int,
  displayedInterfaces: list(string),
  streamLowIndex: int,
  interfaceLowIndex: int,
  verticalScollRef: ref(option(Dom.element)),
  horizontalScollRef: ref(option(Dom.element)),
  queuedActions: list(patchActionQueueItem),
};

type action =
  | LoadChannelMap(list(channelDescription))
  | ChangeGroupSize(int)
  | ChangeDisplayedInterfaces(list(string))
  | LoadNetworkStreams(list(networkStreamDescription))
  | MakeConnection(int, networkStreamDescription)
  | RemoveConnection(int)
  | HandlePatchEvent(int, int)
  | GetAdvertisedSessions
  | GetInterfaces;
/* On every register write, have FPGA append register address to FIFO somewhere in memory. Iterate through FIFO and reload state. */

/* METHODS */
let channelsAsString = channels =>
  ListLabels.fold_left(
    ~f=
      (channelString, channel) =>
        string_of_int(channel.id) ++ " " ++ channelString,
    ~init="",
    channels,
  )
  ->String.trim;

/* Take up to n items from list, otherwise return [] */
let rec takeUpTo = (list, n) => {
  switch (n) {
  | 0 => []
  | _ =>
    if (list->List.length < n) {
      list->takeUpTo(n - 1);
    } else {
      list->Belt.List.take(n)->Belt.Option.getWithDefault([]);
    }
  };
};