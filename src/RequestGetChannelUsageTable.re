/*
 open PatchPanelCommon;

 module CapabilityDescription = {
   type t = {
     capability: int,
     aux_string: string,
   };
   let decode = json =>
     Json.Decode.{
       capability: json |> field("capability", int),
       aux_string: json |> field("string", string),
     };
 };
 module InputOutputDescription = {
   type t = {
     name: string,
     redundant_type: string,
     remote_sid: int,
     remote_sid_str: string,
     session: int,
     state: string,
     stream: int,
     stream_hw_id: int,
     stream_name: string,
     stream_type: string,
   };
   let decode = json =>
     Json.Decode.{
       name: json |> field("name", string),
       redundant_type: json |> field("redundant-type", string),
       remote_sid: json |> field("remote-sid", int),
       remote_sid_str: json |> field("remote-sid-str", string),
       session: json |> field("session", int),
       state: json |> field("state", string),
       stream: json |> field("stream", int),
       stream_hw_id: json |> field("stream-hw-id", int),
       stream_name: json |> field("stream-name", string),
       stream_type: json |> field("type", string),
     };
 };
 module MediaTypeDescription = {
   type t = {
     media_type: int,
     aux_string: string,
   };
   let decode = json =>
     Json.Decode.{
       media_type: json |> field("media-type", int),
       aux_string: json |> field("string", string),
     };
 };
 module SessionDescription = {
   type t = {
     blocked: bool,
     capabilities: CapabilityDescription.t,
     id: int,
     input: InputOutputDescription.t,
     media_type: MediaTypeDescription.t,
     name: string,
     name_input: string,
     name_output: string,
     output: InputOutputDescription.t,
   };
   let decode = json =>
     Json.Decode.{
       blocked: json |> field("blocked", bool),
       capabilities:
         json |> field("capabilities", CapabilityDescription.decode),
       id: json |> field("id", int),
       input: json |> field("input", InputOutputDescription.decode),
       media_type: json |> field("media-type", MediaTypeDescription.decode),
       name: json |> field("name", string),
       name_input: json |> field("name-input", string),
       name_output: json |> field("name-output", string),
       output: json |> field("output", InputOutputDescription.decode),
     };
 };
 module ChannelsList = {
   type t = {
     channels: list(SessionDescription.t),
     aux_string: string,
     aux_type: int,
   };
   let decode = json =>
     Json.Decode.{
       channels: json |> field("channels", list(SessionDescription.decode)),
       aux_string: json |> field("string", string),
       aux_type: json |> field("type", int),
     };
 };
 module GetChannelUsageTable = {
   type t = {
     auxmsg: option(string),
     command: string,
     error: int,
     error_string: string,
     success: bool,
     list: list(ChannelsList.t),
   };
   let decode = json =>
     Json.Decode.{
       auxmsg: json |> field("auxmsg", optional(string)),
       command: json |> field("command", string),
       error: json |> field("error", int),
       error_string: json |> field("error_string", string),
       success: json |> field("success", bool),
       list: json |> field("list", list(ChannelsList.decode)),
     };
 };

 type action('a) =
   | Load
   | Loaded('a)
   | LoadFailed;

 type state =
   | NotAsked
   | Loading
   | Failure
   | Success(GetChannelUsageTable.t);

 let component = ReasonReact.reducerComponent("GetChannelUsageTable");

 let make = (~parentSend, _children) => {
   ...component,
   initialState: () => NotAsked,
   reducer: (action, _state) =>
     switch (action) {
     | Load =>
       ReasonReact.UpdateWithSideEffects(
         Loading,
         self =>
           Js.Promise.(
             JSONRequest.jsonRequest(
               ~command="get_channel_usage_table",
               ~arguments=
                 Json.Encode.(object_([("full", Json.Encode.bool(true))])),
               ~decoder=GetChannelUsageTable.decode,
               (),
             )
             |> Js.Promise.then_(result =>
                  switch (result) {
                  | Some(value) =>
                    let interfaces: list(interfaceDescription) =
                      GetChannelUsageTable.(value.list)
                      |> List.map((interface: ChannelsList.t) =>
                           {
                             interface_name: interface.aux_string,
                             channels:
                               interface.channels
                               |> List.map((channel: SessionDescription.t) =>
                                    (
                                      {
                                        id: channel.id,
                                        session_id:
                                          channel.output.state == "Enabled" ?
                                            Some(channel.output.remote_sid) :
                                            None,
                                        local_session_id:
                                          channel.output.state == "Enabled" ?
                                            Some(channel.output.session) : None,
                                      }: channelDescription
                                    )
                                  ),
                           }
                         );
                    let channels_model =
                      GetChannelUsageTable.(value.list)
                      |> List.map((interface: ChannelsList.t) =>
                           interface.channels
                           |> List.map((channel: SessionDescription.t) =>
                                {
                                  interface_name1: interface.aux_string,
                                  id1: channel.id,
                                  associated_stream1:
                                    channel.output.state == "Enabled" ?
                                      Some({
                                        stream_name':
                                          channel.output.stream_name,
                                        number_channels_stream': 0,
                                        remote_session_id':
                                          channel.output.remote_sid,
                                        local_session_id':
                                          channel.output.session,
                                      }) :
                                      None,
                                }
                              )
                         )
                      |> List.flatten;
                    Js.log(channels_model |> Array.of_list);
                    resolve(
                      parentSend(
                        LoadChannelMap(channels_model->Array.of_list),
                      ),
                    );
                  | None => resolve(self.send(LoadFailed))
                  }
                )
             |> ignore
           ),
       )
     | Loaded(json) => ReasonReact.Update(Success(json))
     | LoadFailed => ReasonReact.Update(Failure)
     },
   render: self =>
     switch (self.state) {
     | NotAsked =>
       <div>
         <a
           className="btn btn-outline-primary"
           onClick={_event => self.send(Load)}>
           {ReasonReact.string("Load")}
         </a>
       </div>
     | Loading => ReasonReact.null
     | Failure => ReasonReact.null
     | Success(value) => ReasonReact.null
     /* The channel list we get back is a list of entries which contain the interface name and the list of channels under it */
     /*
      let interfaces: list(interfaceDescription) =
        value.list
        |> List.map((interface: ChannelsList.t) => {
             let interfaceDesc: interfaceDescription = {
               interface_name: interface.aux_string,
               channels:
                 interface.channels
                 |> List.map((channel: SessionDescription.t) =>
                      (
                        {
                          id: channel.id,
                          session_id:
                            channel.input.state == "Enabled" ?
                              Some(channel.input.session) : None,
                        }: channelDescription
                      )
                    ),
             };
             interfaceDesc;
           });

      interfaces
      |> List.map(interface =>
           <div
             style={ReactDOMRe.Style.make(
               ~transform="rotate(-90deg) translate(-50%)",
               ~textAlign="center",
               ~display="block",
               ~marginTop="-50%",
               ~transformOrigin="center",
               ~padding="50% 0",
               ~height="0",
               (),
             )}>
             {ReasonReact.string(
                interface.interface_name
                ++ " "
                ++ string_of_int(snd(interface.channel_range))
                ++ " channels",
              )}
           </div>
         )
      |> Array.of_list
      |> ReasonReact.array;
      */
     },
   didMount: self => self.send(Load),
 };
 */