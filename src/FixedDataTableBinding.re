[@bs.module "Src/FixedDataTable.jsx"]
external _FixedDataTable: ReasonReact.reactClass = "default";

[@bs.deriving abstract]
type jsProps = {
  interfaces: Js.Json.t,
  streams: Js.Json.t,
  parentClickListener: (int, int) => unit,
};

let make = (~interfaces, ~streams, ~parentClickListener, children) => {
  Js.log(children);
  ReasonReact.wrapJsForReason(
    ~reactClass=_FixedDataTable,
    ~props=jsProps(~interfaces, ~streams, ~parentClickListener),
    children,
  );
};