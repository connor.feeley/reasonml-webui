module ClockConfig = {
  type t = {
    ref_str: string,
    ref_type: int,
  };
  let decode = json =>
    Json.Decode.{
      ref_str: json |> field("ref-str", string),
      ref_type: json |> field("ref-type", int),
    };
};
module ClockCdcCounter = {
  type t = {
    current: int,
    max: int,
    min: int,
    overflow_event: bool,
    underflow_event: bool,
  };
  let decode = json =>
    Json.Decode.{
      current: json |> field("current", int),
      max: json |> field("max", int),
      min: json |> field("min", int),
      overflow_event: json |> field("overflow-event", bool),
      underflow_event: json |> field("underflow-event", bool),
    };
};
module ClockPtpCounter = {
  type t = {average_ofm: int};
  let decode = json =>
    Json.Decode.{average_ofm: json |> field("average-ofm", int)};
};
module ClockCounters = {
  type t = {
    cdc: ClockCdcCounter.t,
    ptp: ClockPtpCounter.t,
  };
  let decode = json =>
    Json.Decode.{
      cdc: json |> field("cdc", ClockCdcCounter.decode),
      ptp: json |> field("ptp", ClockPtpCounter.decode),
    };
};
module ClockStatus = {
  type t = {
    code: int,
    status_string: string,
  };
  let decode = json =>
    Json.Decode.{
      code: json |> field("code", int),
      status_string: json |> field("string", string),
    };
};
module SystemClockStatus = {
  type t = {
    current_ref: ClockStatus.t,
    interface: ClockStatus.t,
    system: ClockStatus.t,
  };
  let decode = json =>
    Json.Decode.{
      current_ref: json |> field("current-ref", ClockStatus.decode),
      interface: json |> field("interface", ClockStatus.decode),
      system: json |> field("system", ClockStatus.decode),
    };
};
module SystemClockInfo = {
  type t = {
    auxmsg: option(string),
    command: string,
    error: int,
    error_string: string,
    success: bool,
    config: ClockConfig.t,
    counters: ClockCounters.t,
    status: SystemClockStatus.t,
  };
  let decode = json =>
    Json.Decode.{
      auxmsg: json |> field("auxmsg", optional(string)),
      command: json |> field("command", string),
      error: json |> field("error", int),
      error_string: json |> field("error_string", string),
      success: json |> field("success", bool),
      config: json |> field("config", ClockConfig.decode),
      counters: json |> field("counters", ClockCounters.decode),
      status: json |> field("status", SystemClockStatus.decode),
    };
};

type action('a) =
  | Load
  | Loaded('a)
  | LoadFailed;

type state =
  | NotAsked
  | Loading
  | Failure
  | Success(SystemClockInfo.t);

let component = ReasonReact.reducerComponent("JSONRequest");

let make = _children => {
  ...component,
  initialState: () => NotAsked,
  didMount: self => self.send(Load),
  reducer: (action, _state) =>
    switch (action) {
    | Load =>
      ReasonReact.UpdateWithSideEffects(
        Loading,
        self =>
          Js.Promise.(
            JSONRequest.jsonRequest(
              ~command="get_system_clock_info",
              ~decoder=SystemClockInfo.decode,
              (),
            )
            |> Js.Promise.then_(result =>
                 switch (result) {
                 | Some(value) => resolve(self.send(Loaded(value)))
                 | None => resolve(self.send(LoadFailed))
                 }
               )
            |> ignore
          ),
      )
    | Loaded(json) => ReasonReact.Update(Success(json))
    | LoadFailed => ReasonReact.Update(Failure)
    },
  render: self =>
    switch (self.state) {
    | NotAsked =>
      <div>
        <a
          className="btn btn-outline-primary"
          onClick={_event => self.send(Load)}>
          {ReasonReact.string("Load")}
        </a>
      </div>
    | Loading => <div> {ReasonReact.string("Loading...")} </div>
    | Failure => <div> {ReasonReact.string("Failed")} </div>
    | Success(value) =>
      <TableDisplay>
        <TableRow label="Clock Config">
          {ReasonReact.string(value.config.ref_str)}
        </TableRow>
        <TableRow label="Clock Source">
          {ReasonReact.string(value.status.current_ref.status_string)}
        </TableRow>
        <TableRow label="Average OFM">
          {ReasonReact.string(string_of_int(value.counters.ptp.average_ofm))}
        </TableRow>
      </TableDisplay>
    },
};