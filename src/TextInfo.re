let component = ReasonReact.statelessComponent("TextInfo");

let make = (~title, ~value, _children) => {
  ...component,
  render: _self => {
    <div className="config-text-info">
      <div className="config-text-info-title">
        <b> {ReasonReact.string(title ++ ": ")} </b>
      </div>
      <div className="config-text-info-value">
        {ReasonReact.string(value)}
      </div>
    </div>;
  },
};