open CommonTypes;
open PatchPanelCommon;

[%raw {|require('Styles/PatchPanel.css')|}];

/*
 let encode_channel_list = interface =>
   interface.channels
   |> List.mapi((position, channel: channelDescription) =>
        Json.Encode.(
          object_([
            ("id", channel.id |> int),
            ("position", position |> int),
            ("type", interface.interface_name |> string),
          ])
        )
      );
      */

let channelsToInterfaceGroups = (channels, streamSize) => {
  /* Fold channel list left. If current element interface name is the same as the head of the first list, append it to the previous list. If not, start a new list using the current element and prepend that to the list of lists. */
  let interfaces =
    channels
    |> List.fold_left(
         (
           interfaceGroups: list(list(channelDescription)) /* Intermediate result */,
           channel: channelDescription /* Current element */,
         ) =>
           channel.interface_name
           == interfaceGroups
              ->List.hd
              ->Belt.List.head
              ->Belt.Option.getWithDefault({
                  interface_name: "UNUSED",
                  id: 0,
                  associated_stream: None,
                }).
                interface_name ?
             [
               [
                 channel,
                 ...interfaceGroups
                    ->Belt.List.head
                    ->Belt.Option.getWithDefault([]),
               ],
               ...interfaceGroups
                  ->Belt.List.tail
                  ->Belt.Option.getWithDefault([]),
             ] :
             [[channel], ...interfaceGroups],
         [[]],
       )
    |> List.filter(sublist => List.length(sublist) != 0); /* TODO: find out why we have an empty list. Should be because of getWithDefault. */

  /* For each of my interfaces, fold the channels and split them such that each group's size is less than streamSize */
  /* Fold channels right and split them such that each groups' size is less than streamSize */
  let interfaceGroups =
    interfaces
    |> List.map(interface =>
         List.fold_right(
           (
             channel: channelDescription,
             groupsIntermediate: list(list(channelDescription)),
           ) => {
             let currentGroupSize = groupsIntermediate->List.hd->List.length;
             currentGroupSize < streamSize ?
               /* Prepend channel to head of current group */
               [
                 [channel, ...groupsIntermediate->List.hd],
                 ...groupsIntermediate->List.tl,
               ] :
               /* Create new channel group */
               [[channel], ...groupsIntermediate];
           },
           interface,
           [[]],
         )
       )
    |> List.flatten
    |> List.map((channels: list(channelDescription)) =>
         {interface_name: List.hd(channels).interface_name, channels}
       );
  interfaceGroups;
};

/*
 let encode_cmd_json = (interface, networkStream) =>
   Json.Encode.(
     object_([
       (
         "channel-list",
         encode_channel_list(interface)
         ->takeUpTo(networkStream.number_channels_stream)
         |> Array.of_list
         |> jsonArray,
       ),
       (
         "rem-sid",
         object_([
           ("id", networkStream.session_id |> int),
           ("payload-type", 98 |> int),
           ("media-idx", 0 |> int),
         ]),
       ),
       ("session-id", 0 |> int),
       ("user-sdp", "" |> string),
       (
         "name",
         networkStream.stream_name
         ++ "->"
         ++ interface.interface_name
         ++ " ("
         ++ string_of_int(List.hd(interface.channels).id)
         ++ "-"
         ++ string_of_int(interface.channels->List.rev->List.hd.id)
         ++ ")"
         |> string,
       ),
     ])
   );
   */

let component = ReasonReact.reducerComponent("PatchPanel");

let make = _children => {
  ...component,
  initialState: () => {
    channels_model: None,
    interfaces: None,
    networkStreams: None,
    interfaceGroupSize: 8,
    displayedInterfaces: ["i2s, tdm"],
    streamLowIndex: 0,
    interfaceLowIndex: 0,
    verticalScollRef: ref(None),
    horizontalScollRef: ref(None),
    queuedActions: [],
  },
  reducer: (action, state) =>
    switch (action) {
    | ChangeDisplayedInterfaces(interfaceNames) =>
      ReasonReact.Update({...state, displayedInterfaces: interfaceNames})
    | ChangeGroupSize(size) =>
      ReasonReact.Update({
        ...state,
        interfaceGroupSize: size,
        interfaces:
          switch (state.channels_model) {
          | Some(channels) =>
            Some(
              channelsToInterfaceGroups(channels->Array.to_list, size)
              ->List.rev
              ->Array.of_list,
            )
          | None => None
          },
      })
    | LoadChannelMap(channels) =>
      ReasonReact.Update({
        ...state,
        channels_model: Some(channels->Array.of_list),
        interfaces:
          Some(
            channelsToInterfaceGroups(channels, state.interfaceGroupSize)
            ->List.rev
            ->Array.of_list,
          ),
      })
    | LoadNetworkStreams(streams) =>
      ReasonReact.Update({
        ...state,
        networkStreams: Some(streams->Array.of_list),
      })
    | HandlePatchEvent(x_index, y_index) =>
      switch (state.networkStreams, state.interfaces) {
      | (Some(streams), Some(interfaces)) =>
        let selectedStream = streams[y_index];
        let selectedInterface = interfaces[x_index];
        Js.log(
          "Corresponding to stream name '"
          ++ selectedStream.stream_name
          ++ "' and interface '"
          ++ selectedInterface.interface_name
          ++ "' with channels "
          ++ channelsAsString(selectedInterface.channels),
        );
        ReasonReact.Update({
          ...state,
          queuedActions: [
            {stream: selectedStream, interface: selectedInterface},
            ...state.queuedActions,
          ],
        });
      | (_, _) => ReasonReact.NoUpdate
      }
    | MakeConnection(interface, networkStream) =>
      /* Have to create a JSON message to execute the connection */
      ReasonReact.UpdateWithSideEffects(
        state,
        _self => (),
        /*
         let request_args = encode_cmd_json(interface, networkStream);
         Js.Promise.(
           JSONRequest.jsonRequest(
             ~command="stream_add_destination",
             ~arguments=request_args,
             ~decoder=CommonTypes.BaseResult.decode,
             (),
           )
           |> Js.Promise.then_(result => {
                Js.log(result);
                resolve(result);
              })
           |> ignore
         );
         */
      )
    | RemoveConnection(session_id) =>
      /* Have to create a JSON message to execute the connection */
      ReasonReact.UpdateWithSideEffects(
        state,
        _self =>
          Js.Promise.(
            JSONRequest.jsonRequest(
              ~command="session_remove",
              ~arguments=Json.Encode.(object_([("id", session_id |> int)])),
              ~decoder=CommonTypes.BaseResult.decode,
              (),
            )
            |> Js.Promise.then_(result => {
                 Js.log(result);
                 resolve(result);
               })
            |> ignore
          ),
      )
    | GetAdvertisedSessions =>
      ReasonReact.SideEffects(
        self =>
          Js.Promise.(
            JSONRequest.jsonRequest(
              ~command="get_advertised_session_list",
              ~decoder=GetAdvertisedSessionList.decode,
              (),
            )
            |> Js.Promise.then_(result =>
                 switch (result) {
                 | Some(value) =>
                   switch (GetAdvertisedSessionList.(value.endpoint_list)) {
                   | Some(endpointList) =>
                     let streamDescriptions: list(networkStreamDescription) =
                       endpointList
                       |> List.map((stream: EndpointDescription.t)
                            /* TODO: rename stream to endpoint */
                            =>
                              {
                                stream_name: stream.session.info,
                                number_channels_stream:
                                  List.hd(
                                    List.hd(stream.session.media).rtpmap,
                                  ).
                                    number_channels,
                                remote_session_id: stream.session.id,
                                local_session_id: 0 /* TODO: make this optional! */
                              }
                            );
                     resolve(
                       self.send(LoadNetworkStreams(streamDescriptions)),
                     );
                   | None => resolve()
                   }
                 | None => resolve()
                 }
               )
            |> ignore
          ),
      )
    | GetInterfaces =>
      ReasonReact.SideEffects(
        self =>
          Js.Promise.(
            JSONRequest.jsonRequest(
              ~command="get_channel_usage_table",
              ~arguments=
                Json.Encode.(object_([("full", Json.Encode.bool(true))])),
              ~decoder=GetChannelUsageTable.decode,
              (),
            )
            |> Js.Promise.then_(result =>
                 switch (result) {
                 | Some(value) =>
                   let channels_model =
                     GetChannelUsageTable.(value.list)
                     |> List.map((interface: ChannelsList.t) =>
                          interface.channels
                          |> List.map((channel: ChannelSessionDescription.t) =>
                               {
                                 interface_name: interface.aux_string,
                                 id: channel.id,
                                 associated_stream:
                                   channel.input.state == "Enabled" ?
                                     Some({
                                       stream_name: channel.output.stream_name,
                                       number_channels_stream: 0,
                                       remote_session_id:
                                         channel.input.session,
                                       local_session_id: channel.input.session,
                                     }) :
                                     None,
                               }
                             )
                        )
                     |> List.flatten;
                   resolve(self.send(LoadChannelMap(channels_model)));
                 | None => resolve()
                 }
               )
            |> ignore
          ),
      )
    },
  render: self =>
    <div id="patch-panel">
      <div> <h1> {ReasonReact.string("Patch")} </h1> </div>
      <div>
        <div>
          <span> {ReasonReact.string("Interface group size: ")} </span>
          <select
            onChange={event =>
              self.send(
                ChangeGroupSize(
                  int_of_string(event->ReactEvent.Form.target##value),
                ),
              )
            }>
            <option value="1">
              {ReasonReact.string("1 (warning - slow)")}
            </option>
            <option value="2">
              {ReasonReact.string("2 (warning - slow)")}
            </option>
            <option value="4"> {ReasonReact.string("4")} </option>
            <option value="8" selected=true>
              {ReasonReact.string("8")}
            </option>
            <option value="16"> {ReasonReact.string("16")} </option>
            <option value="32"> {ReasonReact.string("32")} </option>
          </select>
        </div>
        <br />
        {switch (self.state.interfaces, self.state.networkStreams) {
         | (Some(interfaces), Some(networkStreamList)) =>
           Js.log(networkStreamList);
           <div id="patch-container">
             <PatchGridFixed
               parentSend={self.send}
               interfaces
               networkStreamList
               streamSize={self.state.interfaceGroupSize}
               dimension_x={
                 interfaces->Array.length > 32 ? 32 : interfaces->Array.length
               }
               dimension_y={networkStreamList->Array.length}
               verticalScrollRef={self.state.verticalScollRef}
               horizontalScrollRef={self.state.horizontalScollRef}
             />
             <div id="patch-divider" />
             <PatchPanelQueue actions={self.state.queuedActions} />
           </div>;
         | _ => <div> {ReasonReact.string("Loading...")} </div>
         }}
        <div />
      </div>
    </div>,
  didMount: self => {
    self.send(GetInterfaces);
    self.send(GetAdvertisedSessions);
  },
};