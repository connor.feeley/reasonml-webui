let component = ReasonReact.statelessComponent("SidebarIcon");

let make = (~name, ~link, ~currentURL, _children) => {
  ...component,
  render: self =>
    <a
      href=link
      style={ReactDOMRe.Style.make(
        ~backgroundColor={
          currentURL == link ? "grey" : "";
        },
        ~color="white",
        ~padding="1em 1.5em",
        ~textDecoration="none",
        ~textTransform="uppercase",
        ~display="flex",
        ~justifyContent="center",
        ~alignItems="center",
        (),
      )}>
      <div className="sidebar-icon">
        {ReasonReact.string(name->Js.String.get(0))}
      </div>
    </a>,
};