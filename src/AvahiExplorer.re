let component = ReasonReact.statelessComponent("AvahiExplorer");

let make = (~label: string, _children) => {
  ...component,
  render: _self =>
    <div className="form-group row">
      <label htmlFor="exampleInputEmail1" className="col-sm-5 control-label">
        {ReasonReact.string(label)}
      </label>
      <div className="col-sm-5">
        <input
          type_="number"
          className="form-control"
          id=label
          placeholder="0"
        />
      </div>
    </div>,
};