[@bs.module "fixed-data-table-2"]
external _Table: ReasonReact.reactClass = "Table";

[@bs.deriving abstract]
type jsProps = {
  rowsCount: int,
  rowHeight: int,
  height: int,
  width: int,
};

/*
 type cellInfo = {
   columnIndex: int,
   isScrolling: bool,
   isVisible: bool,
   key: string,
   rowIndex: int,
   style: ReactDOMRe.style,
 };

 [@bs.val "Object.assign"]
 external objAssign: (Js.Dict.t(_), ReactDOMRe.style) => ReactDOMRe.style = "";

 let cellRendererWrapper = (cellRenderer, json) => {
   let {style} as cellInfo = cellInfo_decode(json) |> Belt.Result.getExn;

   cellRenderer({...cellInfo, style: objAssign(Js.Dict.empty(), style)});
 };
 */

let make = (~height, ~width, ~rowsCount, ~rowHeight, children) =>
  ReasonReact.wrapJsForReason(
    ~reactClass=_Table,
    ~props=jsProps(~height, ~width, ~rowsCount, ~rowHeight),
    children,
  );
