/*
 open CommonTypes;
 open PatchPanelCommon;

 type action('a) =
   | Load
   | Loaded('a)
   | LoadFailed;

 type state =
   | NotAsked
   | Loading
   | Failure
   | Success(GetAdvertisedSessionList.t);

 let component = ReasonReact.reducerComponent("GetAdvertisedSessionList");

 let make = (~parentSend, _children) => {
   ...component,
   initialState: () => NotAsked,
   reducer: (action, _state) =>
     switch (action) {
     | Load =>
       ReasonReact.UpdateWithSideEffects(
         Loading,
         self =>
           Js.Promise.(
             JSONRequest.jsonRequest(
               ~command="get_advertised_session_list",
               ~decoder=GetAdvertisedSessionList.decode,
               (),
             )
             |> Js.Promise.then_(result =>
                  switch (result) {
                  | Some(value) =>
                    switch (GetAdvertisedSessionList.(value.endpoint_list)) {
                    | Some(endpointList) =>
                      let streamDescriptions: list(networkStreamDescription) =
                        endpointList
                        |> List.map((stream: EndpointDescription.t) =>
                             {
                               stream_name: stream.session.info,
                               number_channels_stream:
                                 List.hd(List.hd(stream.session.media).rtpmap).
                                   number_channels,
                               session_id: stream.session.id,
                             }
                           );
                      resolve(
                        parentSend(
                          LoadNetworkStreams(
                            streamDescriptions->Array.of_list,
                          ),
                        ),
                      );
                    | None => resolve(self.send(LoadFailed))
                    }
                  | None => resolve(self.send(LoadFailed))
                  }
                )
             |> ignore
           ),
       )
     | Loaded(json) => ReasonReact.Update(Success(json))
     | LoadFailed => ReasonReact.Update(Failure)
     },
   render: self =>
     switch (self.state) {
     | NotAsked =>
       <div>
         <a
           className="btn btn-outline-primary"
           onClick={_event => self.send(Load)}>
           {ReasonReact.string("Load")}
         </a>
       </div>
     | Loading => ReasonReact.null
     | Failure => ReasonReact.null
     | Success(result) =>
       switch (result.endpoint_list) {
       | Some(endpoint_list) =>
         let div_style =
           ReactDOMRe.Style.make(
             ~background="#ccccff",
             ~borderRadius="5px",
             ~width="2em",
             ~height="2em",
             (),
           );
         /*let div_style = ReactDOMRe.Style.make(());*/
         <div
           style={ReactDOMRe.Style.make(
             ~display="grid",
             ~gridTemplateColumns="12fr repeat(15, 1fr)",
             ~gridGap="0.5em",
             (),
           )}>
           {endpoint_list
            |> List.map(endpoint =>
                 [
                   <div>
                     {ReasonReact.string(
                        EndpointDescription.(endpoint.session).info,
                      )}
                   </div>,
                   <div style=div_style>
                     {ReasonReact.string(string_of_int(2))}
                   </div>,
                   <div style=div_style>
                     {ReasonReact.string(string_of_int(2))}
                   </div>,
                   <div style=div_style>
                     {ReasonReact.string(string_of_int(2))}
                   </div>,
                   <div style=div_style>
                     {ReasonReact.string(string_of_int(2))}
                   </div>,
                   <div style=div_style>
                     {ReasonReact.string(string_of_int(2))}
                   </div>,
                   <div style=div_style>
                     {ReasonReact.string(string_of_int(2))}
                   </div>,
                   <div style=div_style>
                     {ReasonReact.string(string_of_int(2))}
                   </div>,
                   <div style=div_style>
                     {ReasonReact.string(string_of_int(2))}
                   </div>,
                   <div style=div_style>
                     {ReasonReact.string(string_of_int(2))}
                   </div>,
                   <div style=div_style>
                     {ReasonReact.string(string_of_int(2))}
                   </div>,
                   <div style=div_style>
                     {ReasonReact.string(string_of_int(2))}
                   </div>,
                   <div style=div_style>
                     {ReasonReact.string(string_of_int(2))}
                   </div>,
                   <div style=div_style>
                     {ReasonReact.string(string_of_int(2))}
                   </div>,
                   <div style=div_style>
                     {ReasonReact.string(string_of_int(2))}
                   </div>,
                   <div style=div_style>
                     {ReasonReact.string(string_of_int(2))}
                   </div>,
                 ]
                 |> Array.of_list
                 |> ReasonReact.array
               )
            /*render(EndpointDescription.(endpoint.session))*/
            |> Array.of_list
            |> ReasonReact.array}
         </div>;
       | None => ReasonReact.null
       }
     /*
       ReasonReact.string(result.endpoint_list |> List.length |> string_of_int)
      */
     },
   didMount: self => self.send(Load),
 };
 */