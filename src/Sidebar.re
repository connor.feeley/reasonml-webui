type state = {collapsed: bool};
type action =
  | Toggle;

let component = ReasonReact.reducerComponent("Sidebar");
let make = (~currentURL, _children) => {
  ...component,
  initialState: () => {collapsed: false},
  reducer: (action, state) =>
    ReasonReact.Update({collapsed: !state.collapsed}),
  render: self =>
    <div
      className="body-sidebar"
      style={ReactDOMRe.Style.make(
        ~display="flex",
        ~flexDirection="column",
        (),
      )}>
      {switch (self.state.collapsed) {
       | false =>
         [
           <a
             style={ReactDOMRe.Style.make(
               ~color="white",
               ~padding="1em 1.5em",
               ~textDecoration="none",
               ~textTransform="uppercase",
               ~display="flex",
               ~justifyContent="center",
               ~alignItems="center",
               (),
             )}>
             <div
               className="sidebar-icon"
               onClick={_event => self.send(Toggle)}
               style={ReactDOMRe.Style.make(
                 ~backgroundColor="grey",
                 ~color="white",
                 ~padding="1em 1.5em",
                 (),
               )}>
               {ReasonReact.string("<<")}
             </div>
           </a>,
           <SidebarLink name="Home" link="#" currentURL />,
           <SidebarLink name="Patch" link="#patch" currentURL />,
           <SidebarLink name="Cloud" link="#cloud" currentURL />,
           <SidebarLink name="Source" link="#source" currentURL />,
           <SidebarLink name="Destination" link="#destination" currentURL />,
           <SidebarLink name="Sync" link="#sync" currentURL />,
           <SidebarLink name="Config" link="#config" currentURL />,
         ]
         ->Array.of_list
         ->ReasonReact.array
       | true =>
         [
           <a
             style={ReactDOMRe.Style.make(
               ~color="white",
               ~padding="1em 1.5em",
               ~textDecoration="none",
               ~textTransform="uppercase",
               ~display="flex",
               ~justifyContent="center",
               ~alignItems="center",
               (),
             )}>
             <div
               className="sidebar-icon"
               onClick={_event => self.send(Toggle)}
               style={ReactDOMRe.Style.make(
                 ~backgroundColor="grey",
                 ~color="white",
                 ~padding="1em 1.5em",
                 (),
               )}>
               {ReasonReact.string(">>")}
             </div>
           </a>,
           <SidebarIcon name="Home" link="#" currentURL />,
           <SidebarIcon name="Patch" link="#patch" currentURL />,
           <SidebarIcon name="Cloud" link="#cloud" currentURL />,
           <SidebarIcon name="Source" link="#source" currentURL />,
           <SidebarIcon name="Destination" link="#destination" currentURL />,
           <SidebarIcon name="Sync" link="#sync" currentURL />,
           <SidebarIcon name="Config" link="#config" currentURL />,
         ]
         ->Array.of_list
         ->ReasonReact.array
       }}
    </div>,
};