open PatchPanelCommon;

let component = ReasonReact.statelessComponent("PachGridInterfaceHeader");

let make =
    (
      ~interface_name,
      ~channel_number_low,
      ~channel_number_high,
      ~connected,
      _children,
    ) => {
  {
    ...component,
    render: self => {
      let className =
        connected ?
          "interface-header-name patch-grid-cell-header-inuse" :
          "interface-header-name";

      <div className="interface-header-container">
        <div className>
          {ReasonReact.string(String.uppercase(interface_name))}
        </div>
        <div className="interface-header interface-header-channel-count">
          {ReasonReact.string(
             " ("
             ++ string_of_int(channel_number_low)
             ++ "-"
             ++ string_of_int(channel_number_high)
             ++ ")",
           )}
        </div>
      </div>;
    },
  };
};