open SimulatedApi;

let simulate_api = false;

let ip = "127.0.0.1";
let port = "48884";
let url = "http://" ++ ip ++ ":" ++ port ++ "/cgi-bin/handleCommands";

let jsonRequest =
    (~command: string, ~arguments=?, ~decoder: Js.Json.t => 'a, ()) => {
  let payload =
    "{\"command\":\""
    ++ command
    ++ "\", \"json\":"
    ++ (
      switch (arguments) {
      | None => "{}"
      | Some(args) => Json.stringify(args)
      }
    )
    ++ "}";
  Js.log(payload);

  if (simulate_api) {
    if (command == "get_advertised_session_list") {
      Js.Promise.(
        Json.parseOrRaise(get_advertised_session_list_simulated)
        ->(
            json => {
              Js.log(json);
              resolve(json);
            }
          )
        |> then_(json => json->decoder->(result => Some(result)->resolve))
      );
    } else if (command == "get_channel_usage_table") {
      Js.Promise.(
        Json.parseOrRaise(get_channel_usage_table_simulated)
        ->(
            json => {
              Js.log(json);
              resolve(json);
            }
          )
        |> then_(json => json->decoder->(result => Some(result)->resolve))
      );
    } else {
      Js.Promise.(resolve(None));
    };
  } else {
    Js.Promise.(
      Fetch.fetchWithInit(
        url,
        Fetch.RequestInit.make(
          ~method_=Post,
          ~body=Fetch.BodyInit.make(payload),
          ~headers=
            Fetch.HeadersInit.make({"Content-Type": "application/json"}),
          (),
        ),
      )
      |> then_(Fetch.Response.json)
      |> then_(json => {
           Js.log(json);
           resolve(json);
         })
      |> then_(json => json |> decoder |> (result => Some(result) |> resolve))
    );
  };
};
