open BsReactstrap;

let component = ReasonReact.statelessComponent("InfoPane");

let make = (~title: string, _children) => {
  ...component,
  render: _self =>
    <div className="col-auto">
      <Card>
        <CardBody>
          <CardTitle> {ReasonReact.string(title)} </CardTitle>
        </CardBody>
        <CardText> {_children |> ReasonReact.array} </CardText>
      </Card>
      <br />
    </div>,
};