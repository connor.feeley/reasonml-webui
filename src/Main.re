[%%debugger.chrome];
[@bs.module] external logo: string = "Images/logo.png";

[%raw {|require('Styles/Main.css')|}];

type route =
  | Home
  | Patch
  | Cloud
  | Source
  | Destination
  | Sync
  | Config;

let routeName_to_string = (theRoute: route) =>
  switch (theRoute) {
  | Home => "Home"
  | Patch => "Patch"
  | Cloud => "Cloud"
  | Source => "Source"
  | Destination => "Destination"
  | Sync => "Sync"
  | Config => "Config"
  };

let routeLink_to_string = (theRoute: route) =>
  switch (theRoute) {
  | Home => "#home"
  | Patch => "#patch"
  | Cloud => "#cloud"
  | Source => "#source"
  | Destination => "#destination"
  | Sync => "#sync"
  | Config => "#config"
  };

type state = {route};

type action =
  | ChangeRoute(route);

let component = ReasonReact.reducerComponent("Main");

let handleClick = (_event, _self) => Js.log("clicked!");

let urlToRoute = (url: ReasonReact.Router.url) =>
  switch (url.hash) {
  | "" => Home
  | "patch" => Patch
  | "cloud" => Cloud
  | "source" => Source
  | "destination" => Destination
  | "sync" => Sync
  | "config" => Config
  | _ => Home
  };

/* Websocket updating requires a daemon running on the board to forward ZMQ messages over a websocket
open WebsocketClient;

let ws = Websocket.make("ws://" ++ JSONRequest.ip ++ ":9001"); /*, ~protocols=[| "protocolOne" |]);*/

Websocket.onOpen(
 ws,
 _ => {
   Js.log("Opened websocket");
   Websocket.send(ws, "Websocket opened");
 },
);

Websocket.onMessage(
 ws,
 ev => {
   Js.log({j|Message received: $ev|j});
   Js.log(WebsocketClient.MessageEvent.data(ev));
 },
);

Websocket.onError(
 ws,
 ev => {
   Js.log(ev);
   Websocket.close(ws);
 },
);

Websocket.onClose(ws, ev => Js.log(ev));
*/

let make = _children => {
  ...component,
  initialState: () => {route: Home},
  reducer: (action, _state) =>
    switch (action) {
    | ChangeRoute(route) => ReasonReact.Update({route: route})
    },
  didMount: self => {
    let watcherID =
      ReasonReact.Router.watchUrl(url =>
        self.send(ChangeRoute(url |> urlToRoute))
      );
    self.onUnmount(() => ReasonReact.Router.unwatchUrl(watcherID));
  },
  render: self =>
    <div className="body-container">
      <Sidebar currentURL={routeLink_to_string(self.state.route)} />
      <div className="body-content">
        {switch (self.state.route) {
         | Home =>
           <div>
             <div> {ReasonReact.string("Home: unimplemented")} </div>
           </div>
         | Patch => <PatchPanel />
         | Cloud => <CloudPanel />
         | Source => <SourcePanel />
         | Destination => <DestinationPanel />
         | Sync => <SyncPanel />
         | Config => <SettingsPanel />
         }}
      </div>
    </div>,
};
