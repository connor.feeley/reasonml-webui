open BsReactstrap;

module NetworkInterface = {
  type t = {
    ipv4_address: string,
    tcp_port: int,
  };
  let decode = json =>
    Json.Decode.{
      ipv4_address: json |> field("ipv4", string),
      tcp_port: json |> field("tcp", int),
    };
};
module NetworkDevice = {
  type t = {
    addr_list: list(NetworkInterface.t),
    hostname: string,
  };
  let decode = json =>
    Json.Decode.{
      addr_list: json |> field("addr", list(NetworkInterface.decode)),
      hostname: json |> field("hostname", string),
    };
};
module GetNetworkDevicesList = {
  type t = {
    auxmsg: option(string),
    command: string,
    error: int,
    error_string: string,
    success: bool,
    list: option(list(NetworkDevice.t)),
  };
  let decode = json =>
    Json.Decode.{
      auxmsg: json |> field("auxmsg", optional(string)),
      command: json |> field("command", string),
      error: json |> field("error", int),
      error_string: json |> field("error_string", string),
      success: json |> field("success", bool),
      list: json |> field("list", optional(list(NetworkDevice.decode))),
    };
};

type action('a) =
  | Load
  | Loaded('a)
  | LoadFailed;

type state =
  | NotAsked
  | Loading
  | Failure
  | Success(GetNetworkDevicesList.t);

let component = ReasonReact.reducerComponent("GetNetworkDevices");

let make = _children => {
  ...component,
  initialState: () => NotAsked,
  reducer: (action, _state) =>
    switch (action) {
    | Load =>
      ReasonReact.UpdateWithSideEffects(
        Loading,
        self =>
          Js.Promise.(
            JSONRequest.jsonRequest(
              ~command="get_network_devices_list",
              ~decoder=GetNetworkDevicesList.decode,
              (),
            )
            |> Js.Promise.then_(result =>
                 switch (result) {
                 | Some(value) => resolve(self.send(Loaded(value)))
                 | None => resolve(self.send(LoadFailed))
                 }
               )
            |> ignore
          ),
      )
    | Loaded(json) => ReasonReact.Update(Success(json))
    | LoadFailed => ReasonReact.Update(Failure)
    },
  render: self =>
    switch (self.state) {
    | NotAsked =>
      <div>
        <a
          className="btn btn-outline-primary"
          onClick={_event => self.send(Load)}>
          {ReasonReact.string("Load")}
        </a>
      </div>
    | Loading => <div> {ReasonReact.string("Loading...")} </div>
    | Failure => <div> {ReasonReact.string("Failed")} </div>
    | Success(value) =>
      switch (value.list) {
      | Some(list) =>
        list
        |> List.map(device =>
             <div>
               <Card>
                 <CardBody>
                   <CardTitle>
                     {ReasonReact.string(NetworkDevice.(device.hostname))}
                   </CardTitle>
                   <CardText>
                     ...{
                          device.addr_list
                          |> List.map(interface =>
                               <p>
                                 {ReasonReact.string(
                                    NetworkInterface.(
                                      "IP: "
                                      ++ interface.ipv4_address
                                      ++ " Port: "
                                      ++ string_of_int(interface.tcp_port)
                                    ),
                                  )}
                               </p>
                             )
                          |> Array.of_list
                          |> ReasonReact.array
                        }
                   </CardText>
                 </CardBody>
               </Card>
               <br />
             </div>
           )
        |> Array.of_list
        |> ReasonReact.array
      | None => ReasonReact.null
      }
    },
  didMount: self => self.send(Load),
};