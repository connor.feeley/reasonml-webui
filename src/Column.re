[@bs.module "fixed-data-table-2"]
external _Column: ReasonReact.reactClass = "Column";

[@bs.deriving abstract]
type jsProps = {
  header: ReasonReact.reactElement,
  columnKey: string,
  data: array(string),
  width: int,
};

let make = (~header, ~columnKey, ~data, ~width, children) => {
  Js.log(children);
  ReasonReact.wrapJsForReason(
    ~reactClass=_Column,
    ~props=jsProps(~header, ~columnKey, ~data, ~width),
    children |> Array.mapi((index, child) => <Cell rowIndex=index data />),
  );
};