let component = ReasonReact.statelessComponent("SidebarLink");

let make = (~name, ~link, ~currentURL, _children) => {
  ...component,
  render: self =>
    <a
      href=link
      style={ReactDOMRe.Style.make(
        ~backgroundColor={
          currentURL == link ? "grey" : "";
        },
        ~color="white",
        ~padding="1em 1.5em",
        ~textDecoration="none",
        ~textTransform="uppercase",
        (),
      )}>
      {ReasonReact.string(name)}
    </a>,
};