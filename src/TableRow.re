let component = ReasonReact.statelessComponent("TableRow");

let make = (~label: string, _children) => {
  ...component,
  render: _self =>
    <tr>
      <td> <strong> {ReasonReact.string(label)} </strong> </td>
      {_children |> Array.map(child => <td> child </td>) |> ReasonReact.array}
    </tr>,
};