let component = ReasonReact.statelessComponent("TextInput");

let make = (~title, ~value, _children) => {
  ...component,
  render: _self => {
    <div>
      <div> <b> {ReasonReact.string(title)} </b> </div>
      <div> <input type_="text" value /> </div>
    </div>;
  },
};