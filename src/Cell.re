[@bs.module "fixed-data-table-2"]
external _Cell: ReasonReact.reactClass = "Cell";

[@bs.deriving abstract]
type jsProps = {
  rowIndex: int,
  data: array(string),
};

let make = (~rowIndex, ~data, children) =>
  ReasonReact.wrapJsForReason(
    ~reactClass=_Cell,
    ~props=jsProps(~rowIndex, ~data),
    children,
  );