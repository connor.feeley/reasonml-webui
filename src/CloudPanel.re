let component = ReasonReact.statelessComponent("CloudPanel");

let make = _children => {
  ...component,
  render: _self =>
    <div>
      <div
        className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1> {ReasonReact.string("Cloud")} </h1>
      </div>
      {ReasonReact.string("Cloud Panel")}
      <RequestGetNetworkDevices />
    </div>,
};